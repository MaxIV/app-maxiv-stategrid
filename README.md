# Stategrid

<img align="center" src="https://nox.apps.okd.maxiv.lu.se/widget?package=taurusgui-stategrid"/>

This the State Grid application. It displays the current States of a set of devices to the user in a 2D grid. The grid can be organized in a hierarchical way where each grid cell can either represent one device, or a set of devices in a "sub-grid" that can be opened by clicking the cell. Each sub-grid works exactly like the "main grid" and can have its own sub-grids, and so on. Clicking cell that only represents a single device will open an appropriate panel.

All states "bubble up" to the top level, so that even if there is a whole hierarchy of devices hidden under one grid cell, the "most important" (see below) state will always be the one shown by the cell. This means that the top level grid can be used to monitor a large system for problems, and if needed drilled down to find problematic device(s). So, the whole thing can be viewed as a tree structure where each node is a cell, and the leaf nodes are devices. Each node knows the states of all nodes below it, and displays the most important of those states.


## Dependencies

The stategrid application depends on the UnitedStates device for monitoring. This is because, for a large installation (1000s of devices), it is time consuming - and database intensive - to set up listeners for all devices. It is also useful to have a service that is always running regardless of whether stategrid is restarted, since it provides the timestamp of latest State change for each device, logging of state changes, etc.


## Configuration

The Stategrid is configured with a simple JSON based file format which can be given as an argument to the stategrid command. See "example-config.json". It should be fairly self-explanatory.

Some points worth mentioning:

In a grid, the keys are composed as "COLUMN,ROW" where COLUMN must be the name of a column and vice versa. This decides the position of the cell. The cell can be a device, or contain another grid, and so on. 

Column and row names must be unique to each subgrid (but not necessasily globally), else the position of a cell cannot be determined.

The "state_device" key must be present at the top level of the config file, and point to a UnitedStates device that monitors all devices in the grid. Any devices in the grid that are not available in the data from this device will be shown as "NA" (not available).

"state_priorities" is a list of state names sorted in order of rising importance. It defines which states take precedence if more than one different state is represented by a cell. The most important state present will always be the one displayed. If this is not give, by default the list is:

    ["N/A", "RUNNING", "ON", "OPEN", "EXTRACT","STANDBY", "OFF", "CLOSE", "INSERT", "MOVING", "DISABLE", "INIT", "ALARM", "FAULT", "UNKNOWN"]

This means that UNKNOWN state trumps FAULT which trumps ALARM, and so on. States missing from the list will be seen as less important, with undefined relative order.

"state_priorities" can be set on any grid at any level, and will then be used for all underlying grids unless overridden again there. 

Separators can be displayed between any rows or columns by inserting a 'null' (or an empty string) in the "column\_names" or "row\_names" list. It will be displayed simply as a thicker border between the cells. This is purely visual and intended for increasing readability by grouping things together.

The "command" feature is somewhat raw at the moment. The idea is to allow adding menus to cells, rows and columns, that allow running selected Tango commands on all devices they contain. It's not fully implemented and hardly tested, so don't use it, anything may happen if you do.

## Making changes to the configuration for R1 and R3
Start by running the dump_tango.py script in the scripts directory. This will dump the existing configuration from the tango db into json files for each subsystem. 
The json-files are used as input to the configuration script, make_rX_config.py.
Make your changes to the configuration in this script and then run it like this:
python make_r3_config.py > ../stategrid/config/r3.json

## Making changes to the configuration for the Linac
Make your changes in /stategrid/config/linac.py. The stategrid will be automatically generated when running the startup script ctlinacstategrid.

## Filtering devices from stategid without editing files
By adding the **FilterDevices** tango property with a user specified device list under the respected top level UnitedStates device (**I/CTL/STATE, R3/CTL/STATE, R1/CTL/STATE**) you can remove any unwanted device from the stategrid. There is also an ansible playbook tangoconfig.yml to help with the configuration of this feature.

## Invalid Date Error (NA)
This is solved by enabling polling on State attribute of the underlying device.
