#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="taurusgui-stategrid",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description="GUI to show the collected State of the control system.",
    author="KITS SW",
    author_email="kits-sw@maxiv.lu.se",
    license="GPLv3",
    url="http://www.maxlab.lu.se",
    include_package_data=True,
    packages=find_packages(),
    entry_points={
        'console_scripts': ['ctlinacstategrid = stategrid.grid:start_linac',
                            'ctr1stategrid = stategrid.grid:start_r1',
                            'ctr3stategrid = stategrid.grid:start_r3',
                            'stategrid = stategrid.grid:main'
                            ]
    },
    data_files=[
        ('/usr/share/applications', ['maxiv-linacgrid.desktop', 'maxiv-r3grid.desktop', 'maxiv-r1grid.desktop'])],
    install_requires=['taurusgui-magnetpanel', 'taurus', 'pyqtwebengine', 'lxml'],
)
