"""
This script generates a JSON stategrid config for the 3GeV storage
ring at MAXIV. The output should be redirected to a file that can then
be used as argument to the stategrid application.
"""

from itertools import chain, product
import json
import re
import sys

#import tango

# These files are expected to be JSON format with device names as keys
# and anything as values (those are ignored). They should contain all
# devices that should be in the grid.
# They are identical to the files used by the r3synoptic, generated
# from various XLS files.

devs = {}

with open("r3_magnets.json") as f:
    magnets = json.load(f)
    devs.update(magnets)

with open("r3_vac.json") as f:
    vacs = json.load(f)
    devs.update(vacs)

with open("r3_bpm.json") as f:
    bpms = json.load(f)
    devs.update(bpms)

with open("r3_wat.json") as f:
    devs.update(json.load(f))

with open("r3_alarms.json") as f:
    pyalarms = json.load(f)


# # ignore some devices (e.g. fast corrector magnets)
devices_to_skip = [re.compile(p, flags=re.IGNORECASE)
                   for p in [
                           "^R3-3.*\/MAG\/COG.*$",  # fast correctors, not implemented
                           "^R3-301L/VAC/IPFE-01$", # Added manually, this pump should be in the vac column and not in the FE Pumps column
                           "^R3-302S2/VAC/IPFE-01$"  # non-existent pump, should be removed from TANGO
                   ]]


for regex in devices_to_skip:
    for device in devs.keys():
        if regex.match(device):
            del devs[device]

# skip various valves that are actually in beamlines
for device in [
        #"R3-301L/VAC/VGMB-01",  # this one is in fact in TR3
        "R3-302U1/VAC/VGMB-01",
        "R3-303U1/VAC/VGMB-01",
        "R3-304U1/VAC/VGMB-01",
        "R3-305U1/VAC/VGMB-01",
        "R3-306U1/VAC/VGMB-01",
        "R3-307U1/VAC/VGMB-01",
        "R3-308U1/VAC/VGMB-01",
        "R3-309U1/VAC/VGMB-01",
        "R3-310U1/VAC/VGMB-01",
        "R3-311U1/VAC/VGMB-01",
        "R3-312U1/VAC/VGMB-01",
        "R3-313U1/VAC/VGMB-01",
        "R3-314U1/VAC/VGMB-01",
        "R3-315U1/VAC/VGMB-01",
        "R3-316U1/VAC/VGMB-01",
        "R3-317U1/VAC/VGMB-01",
        "R3-318U1/VAC/VGMB-01",
        "R3-319U1/VAC/VGMB-01",
        "R3-320U1/VAC/VGMB-01"]:
    del devs[device]

# states in order of increasing importance
state_prios = ["N/A",  "ON", "OPEN", "EXTRACT",
               "STANDBY", "OFF", "CLOSE", "INSERT", "RUNNING","MOVING", "DISABLE",
               "INIT", "ALARM", "FAULT", "UNKNOWN"]

# These are used to name columns and rows in the various grid levels
# 'None' indicates a separator
subsystems = ["VAC<br>R Pumps", "VAC<br>R Valves", None,
              "VAC<br>FE Pumps", "VAC<br>FE Inserts", "VAC<br>FE", "VAC<br>VGFA", None, "WAT", None,
              "MAG", None, "RF", None,
              "TIM", None,
              "DIA", "DIA<br>TCO", "DIA<br>BPM", "DIA<br>Inserts","CTL<br>FOFB", "CTL", None, "ID", None, "MPS", "PSS"]

# The ring consists of 20 almost identical sections called achromats
achromats = ["301", "302", "303", "304", "305", None,
             "306", "307", "308", "309", "310", None,
             "311", "312", "313", "314", "315", None,
             "316", "317", "318", "319", "320", None,
             # I guess these things could be handled better...
             None, "PLC", None, "Alarm", None, "Global"]

# in each achromat is a set of "cells" (this is not the right terminology)
cells = [
    "L", None, "M1", "S1", None, "U1", "U2", "U3", "U4", "U5", None, "S2", "M2",
    None, ""  # pick up devices "global" to the achromat (no subsection)
]

# The different types of devices sorted by subsystem
device_types = {
    "MAG": [# "ALARM", None,
        "COAX", "COAY",  # "COGX", "COGY", fast correctors, not connected ATM
        None,
        "DIP", "DIPC", "DIPM", "DIPMC", None,
        "OXX", "OXY", "OYY", None,
        "PMH3", "PMV3", None,
        "QF", "QDE", "QFE", "QFM", None,
        "SXD", "SXDE", "SXFI", "SXFM", "SXFO", None, "COID"
    ],
    "VAC<br>R Pumps": [
        "IPFA", "IPFB", "IPFC", None, "IPGA", "IPGB", None, "IPH", "IPK", "IPN", "IPR", None, "VGC", "VGHE"],
        #"IPFA", "IPFB", "IPFC", "IPFE", None,"IPGA", "IPGB", None, "IPH", "IPK", "IPN"],  #r1 order
    "VAC<br>R Valves": ["VGRA", "VGRB"],
    "VAC<br>FE Pumps": ["IPFE"],
    "VAC<br>FE Inserts": ["HA0", "HAB"],
    "VAC<br>FE": ["FE"],
    "PSS":["PSS"],
    "MPS":["FE", None, "VAC", "MAG", "ID", "WAT"],
    # TODO: what to do about valves that aren't in the ring proper?
    "DIA": ["DCCT"],
    "DIA<br>TCO": ["TCO"],  # Other sensors?
    "DIA<br>BPM": ["BPM"],
    "DIA<br>Inserts": ["SCRN", "SCRNM"],
    "WAT": ["FGE", "FSW", "SHG", "TSE"],
    "RF": ["NUTAQ", "NUTAQDIAGS", "TXA"],
    "TIM": ["EVR", None, "FOUT"],
    # "TIM/BPM": ["BPM"]
    "ID": ["IVUFB","EPUFB"]
}

titles = {
    "SHG": "Shuntgroup",
    "FGE": "Flow gauge",
    "FSW": "Flow switch"
}

# # Not used anymore as we read all the info from JSON files above instead of
# # the Tango DB, keeping this for future reference :)
# db = tango.Database()
# devices = {
#     "MAG": list(m for m in db.get_device_exported_for_class("Magnet")
#                 if m.upper().startswith("R3-")),
#     "VAC": (list(db.get_device_exported("R3-*/VAC/IP*")) +
#             list(db.get_device_exported("R3-*/VAC/VGC-*")) +
#             list(db.get_device_exported("R3-*/VAC/VGHE-*"))),
#     "VAC/VG": list(v for v in db.get_device_exported_for_class("VacuumValve")
#                    if v.upper().startswith("R3-")),
#     "DIA": (list(db.get_device_exported("R3-*/DIA/TCO-*")) +
#     "DIA/Cam": list(db.get_device_exported("R3-*/DIA/BPM-*")),
#     "DIA/Inserts": list(db.get_device_exported("R3-*/DIA/SCRN*"))
# }


_devpatt = {}
_devcache = {}
_devreqs = {}  # just for displaying progress


def get_devices(*args):
    # args = achromat, cell, subsystem, devstart=""):
    # arg = (achromat, cell, subsystem, devstart)
    if args not in _devpatt:
        achromat, cell, subsystem, devstart = args
        pattern = re.compile("R3-{}{}/{}/{}-".format(
            achromat, cell, re.split("/|<br>", subsystem)[0], devstart), re.IGNORECASE)
        _devpatt[args] = pattern
    else:
        pattern = _devpatt[args]
    if pattern not in _devcache:
        matches = [dev for dev in devs if pattern.search(dev)]
        for device in matches:
            if device in magnets:
                print(device, file=sys.stderr)
                data = magnets[device]
                circuit = data["circuit"]
                trim = data.get("trim")
                matches.append(circuit)
                if trim:
                    matches.append(trim)
        _devcache[pattern] = matches
        print("\r%d       " % len(_devcache), end=' ', file=sys.stderr)
    else:
        _devreqs[pattern] = _devreqs.setdefault(args, 1) + 1
    return _devcache[pattern]


def get_device(*args):
    devices = get_devices(*args)
    if len(devices) == 1:
        return devices[0]


def get_snumber(*args):
    dev = get_device(*args)
    if dev:
        return devs[dev].get("s", 0)


def devnr(devicename):
    "get the instance number from a device name"

    pattern = re.compile(r'BPM-EVRX|BPM|GDX$')

    if pattern.match(devicename):
        #if "BPM-STATE" in devicename:  #these have names like r3-301/dia/bpm-state-01
        return int(devicename.split("-")[-1])
    else:
        return int(devicename.split("/")[-1].split("-")[1])


def unique(seq, idfun=None):
    #/MPS order preserving uniquify list
    if idfun is None:
        def idfun(x):
            return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        if marker in seen:
            continue
        seen[marker] = 1
        result.append(item)
    return result


# build the config in one go, using dict comprehensions
config = {

    # This is the main grid
    "title": "3 GeV ring",
    "state_device": "R3/CTL/STATE",  # toplevel UnitedStates device
    "window_size": {"width": 1200, "height": 800},
    # "state_priorities": state_prios,

    "grid": {
        "row_names": achromats,
        "column_names": subsystems,
        "cells": {
            "{},{}".format(subs, ach): {  # column first (should be swapped?)
                # Intermediate grid, each row showing sections within an achromat
                # and columns being device types within a subsystem
                "title": "R3-{}/{}".format(ach, subs.replace("<br>", "/")),
                "grid": {
                    "column_names": device_types.get(subs, []),
                    "row_names": cells,
                    "cells": {
                        "{},{}".format(devtype, cell): {
                            # Lowest level grid, showing individual devices
                            "device": get_device(ach, cell, subs, devtype),
                            "key": get_snumber(ach, cell, subs, devtype),
                            "title": "R3-{}{}/{}/{}".format(ach, cell, subs.split("/")[0], devtype),
                            #"state_priorities": state_prios,
                            "grid": {
                                "column_names": unique(
                                    device.split("/")[-1].split("-")[0]
                                    for device in
                                    get_devices(ach, cell, subs.split("/")[0], devtype)),
                                "row_names": ["%02d" % (x+1) for x in
                                              range(max(devnr(dev) for dev in
                                                         get_devices(ach, cell, subs, devtype)))],

                                "cells": {
                                    "{},{}".format(*device.split("/")[-1].split("-")): {
                                        "title": device,
                                        "device": device,
                                        # "key": devs[device].get("s", 0)  # not all devices have s...
                                    }
                                    for device in get_devices(ach, cell, subs, devtype)
                                }
                            } if len(get_devices(ach, cell, subs, devtype)) > 1 else None
                        } for devtype, cell in product(device_types.get(subs, []), cells)
                          if get_devices(ach, cell, subs, devtype)
                    }
                }
            } for ach, subs in product(achromats, subsystems) if ach and subs
        }
    }
}



# PyAlarms
alarms_domains = { domain.replace("R3-", "").replace("B", "")[0:3] :  [] for
                  domain in set([device.split("/")[0] for device in pyalarms
                                 if not "pss" in device.split("/")[1].lower()])}

columns = ["FE", None, "VAC", "MAG", "ID", "WAT"]

rows = set()

for dev in  pyalarms:
    domain, familly, member = dev.split("/")
    if "pss" in familly.lower():
        continue
    try:
        rows.add(member.split("-")[1])
    except IndexError:
        pass
    alarms_domains[domain.replace("R3-", "").replace("B", "")[0:3] ].append(dev)



def get_alarm_column(dev):
    domain, familly, member = dev.split("/")
    if familly == "PSS":
        return familly
    if domain.startswith("B"):
        return "FE"
    else:
        return familly


def get_alarm_row(dev):
    domain, familly, member = dev.split("/")
    try:
        return member.split("-")[1]
    except IndexError:
        return "01"


for domain, devices in alarms_domains.items():
    config["grid"]["cells"]["MPS,{}".format(domain)] = {
        "grid": {
            "column_names": columns,
            "row_names": sorted(list(rows)),
            "cells":{
                "{},{}".format(get_alarm_column(device),
                        get_alarm_row(device)): {
                            "title": device,
                            "device": device,

            } for device in devices
         }
        }
    }

# PSS
alarms_domains = { domain.replace("R3-", "").replace("B", "")[0:3] :  [] for
                  domain in set([device.split("/")[0] for device in pyalarms
                                 if "pss" in device.split("/")[1].lower()])}

# Add global pyalarms
columns = ["VAC", "MAG", "ID", "WAT"]
config["grid"]["cells"]["MPS,Alarm"]["grid"] = {
    "column_names": columns,
    "row_names": ["01", "02"],
    "cells": {
        "VAC,01": {"device": "R3/VAC/ALARM-01"},
        "VAC,02": {"device": "r3/vac/alarm-02".upper()},
        "MAG,01": {"device": "r3/mag/alarm-01".upper()},
        "ID,01":{"device":"r3/id/alarm-01".upper()},
        "WAT,01":{"device":"r3/wat/alarm-01".upper()},

    }
}


columns = ["PSS"]
#columns = ["PSS"]
config["grid"]["cells"]["PSS,Alarm"]["grid"] = {
    "column_names": columns,
    "row_names": ["01", "02"],
    "cells": {
        "PSS,01": {"device": "R3/PSS/ALARM"},

    }
}

#Add global CTL device for feedback
columns = ["FB", "FF","SOFB", "FOFB"]
config["grid"]["cells"]["CTL,Global"] = {
    "state_priorities": ["N/A", "INIT", "RUNNING", "ON", "OPEN", "EXTRACT","STANDBY", "OFF", "CLOSE", "INSERT", "MOVING", "DISABLE", "ALARM", "FAULT", "UNKNOWN"],
}

config["grid"]["cells"]["CTL,Global"]["grid"] = {
    "column_names": columns,
    "row_names": ["01", "02"],
    "cells": {
        "FB,01": {"device": "r3/ctl/fb-03".upper()},
        "FF,01": {"device": "r3/ctl/fpff-01".upper()},
        "FOFB,01": {"device": "r3/ctl/fofb-01".upper()},
        "FOFB,02": {"device": "r3/ctl/fofb-02".upper()},
        "SOFB,01": {"device": "r3/ctl/sofb-01".upper()},
        "SOFB,02": {"device": "r3/ctl/sofb-02".upper()},
    }
}


# Add R3-301L/VAC/IPFE-01  to the Pumps column
config["grid"]["cells"]["VAC<br>R Pumps,301"]["grid"]["column_names"].insert(0,"IPFE")
config["grid"]["cells"]["VAC<br>R Pumps,301"]["grid"]["cells"]["IPFE,L"] = {"device": "R3-301L/VAC/IPFE-01"}


# Add stuff that isn't in the input files, or doesn't fit the "standard"


# skip various valves that are actually in beamline frontends
PATTERN = re.compile("R3-3(\d\d)(.*)/.*/(.*)-\d\d")
for device in [
        #"R3-301L/VAC/VGMB-01",  # this one is in fact in TR3
        "R3-302U1/VAC/VGMB-01",
        "R3-303U1/VAC/VGMB-01",
        "R3-304U1/VAC/VGMB-01",
        "R3-305U1/VAC/VGMB-01",
        "R3-306U1/VAC/VGMB-01",
        "R3-307U1/VAC/VGMB-01",
        "R3-308U1/VAC/VGMB-01",
        "R3-309U1/VAC/VGMB-01",
        "R3-310U1/VAC/VGMB-01",
        "R3-311U1/VAC/VGMB-01",
        "R3-312U1/VAC/VGMB-01",
        "R3-313U1/VAC/VGMB-01",
        "R3-314U1/VAC/VGMB-01",
        "R3-315U1/VAC/VGMB-01",
        "R3-316U1/VAC/VGMB-01",
        "R3-317U1/VAC/VGMB-01",
        "R3-318U1/VAC/VGMB-01",
        "R3-319U1/VAC/VGMB-01",
        "R3-320U1/VAC/VGMB-01",

        "R3-302S2/VAC/VGMC-01",
        "R3-320S1/VAC/VGMC-01",

        "R3-320S1/VAC/VGMJ-01",
        "R3-320S1/VAC/VGMK-01"]:
    parts = PATTERN.match(device)
    achromat, cell, devtype = parts.groups()
    if "VAC<br>FE Inserts,3%s" % achromat not in config["grid"]["cells"]:
        config["grid"]["cells"]["VAC<br>FE Inserts,3%s" % achromat] = {"grid": {"cells": {}}}
    config["grid"]["cells"]["VAC<br>FE Inserts,3%s" % achromat]["grid"]["column_names"] = [
        "VGMB", "VGMC", "VGMJ", "VGMK", None, "HA0", "HAB", None, "FE"]
    config["grid"]["cells"]["VAC<br>FE Inserts,3%s" % achromat]["grid"]["cells"]["%s,%s" % (devtype, cell)] = {
        "device": device
    }


# "Pulsed" magnets
config["grid"]["cells"]["MAG,301"]["grid"]["cells"]["PMH3,S1"] = {
    "grid": {
        "column_names": ["PMH3", "CRPMH3"],
        "row_names": ["01"],
        "cells": {
            "PMH3,01": {"device": "R3-301S1/MAG/PMH3-01"},
            "CRPMH3,01": {"device": "R3-301S1/MAG/CRPMH3-01"}
        }
    }
}
config["grid"]["cells"]["MAG,310"]["grid"]["cells"]["PMV3,S2"] = {
    "grid": {
        "column_names": ["PMV3", "CRPMV3"],
        "row_names": ["01"],
        "cells": {
            "PMV3,01": {"device": "R3-310S2/MAG/PMV3-01"},
            "CRPMV3,01": {"device": "R3-310S2/MAG/CRPMV3-01"}
        }
    }
}


# Trim circuits

# Note that these are actually in the master magnet config sheet, it's
# just that a lot of them are not physically installed so we don't
# want to add them all.  Therefore, we're adding them manually, for
# now. We could probably come up with a better way to handle this,

trim_circuits = [
    "R3-306U2/MAG/CRTSXD-02",
    "R3-306U3/MAG/CRTSXD-01",
    "R3-306U3/MAG/CRTSXD-02",
    "R3-306U4/MAG/CRTSXD-01",

    # "R3-320M1/MAG/CRTOXY-01",
    "R3-320U1/MAG/CRTSXD-01",
    "R3-320U1/MAG/CRTSXD-02",
    "R3-320U2/MAG/CRTSXD-01",
    "R3-320U2/MAG/CRTSXD-02",
    "R3-320U3/MAG/CRTSXD-01"
]

for tc in trim_circuits:
    achromat, cell, magtype, number = re.match(
        "R3-(\d{3})(.{2})/MAG/CRT([^-]+)-(\d{2})", tc).groups()
    grid = (config["grid"]["cells"]["MAG,%s" % achromat]
            ["grid"]["cells"]["%s,%s" % (magtype, cell)]
            ["grid"])
    crtype = "CRT%s" % magtype

    if crtype not in grid["column_names"]:
        # add the column heading if it isn't there
        grid["column_names"].append(crtype)

    grid["cells"]["CRT%s,%s" % (magtype, number)] = {"device": tc}


# VAC PLCs
achromats = ["%02d" % a for a in range(1, 21)]
vac_plcs = ["PLC-01", "PLC-02"]
config["grid"]["cells"]["VAC<br>R Valves,PLC"] = {
    "title": "VAC PLC",
    "grid": {
        "column_names": vac_plcs,
        "row_names": achromats,
        "cells": {
            "{},{}".format(plc, ach): {"device": "R3-3%s/VAC/%s" % (ach, plc)}
            for plc, ach in chain(product(vac_plcs[:1], achromats[:10]),
                                  product(vac_plcs[1:], achromats[10:]))
        }
    }
}

# MAG PLCs
magnet_plc_systems = ["MAG", "SWB"]
config["grid"]["cells"]["MAG,PLC"] = {
    "title": "MAG PLC",
    "grid": {
        "column_names": magnet_plc_systems,
        "row_names": achromats,
        "cells": {
            "{},{}".format(system, ach): {
                "device": "R3-3%s/MAG/PLC-01" % ach if system == "MAG"
                else "R3-A11%s11/MAG/PLC-02" % ach
            } for system, ach in product(magnet_plc_systems, achromats)
        }
    }
}

# ID PLC - only some achromats have them
config["grid"]["cells"]["ID,PLC"] = {
    "title": "ID PLC",
    "grid": {
        "column_names": ["ID"],
        "row_names": ["R3","303","304","308","309","310","312","315","316","317","318"],
        "cells": {
            "ID,R3" : {"device": "R3/ID/PLC-01"},
            "ID,303" : {"device": "R3-303/ID/PLC-01"},
            "ID,304" : {"device": "R3-304/ID/PLC-01"},
            "ID,308" : {"device": "R3-308/ID/PLC-01"},
            "ID,309" : {"device": "R3-309/ID/PLC-01"},
            "ID,310" : {"device": "R3-310/ID/PLC-01"},
            "ID,312" : {"device": "R3-312/ID/PLC-01"},
            "ID,315" : {"device": "R3-315/ID/PLC-01"},
            "ID,316" : {"device": "R3-316/ID/PLC-01"},
            "ID,317" : {"device": "R3-317/ID/PLC-01"},
            "ID,318" : {"device": "R3-318/ID/PLC-01"},
        }
    }
}

# MPS

# PS-MAG-MONITOR PyAlarm (monitors temperature changes through resistance)
config["grid"]["cells"]["MAG,Alarm"] = {
    "title": "MAG Alarm",
    "device": "R3/MAG/PS-ALARM"
}

# # MAG PyAlarm
# for ach in range(1, 21):
#     achromat = "3%02d" % ach
#     pyalarm_device = "R3-%s/MAG/PLC-01-ALARM" % achromat
#     config["grid"]["cells"]["MAG,%s" % achromat]["grid"]["cells"]["ALARM,L"] = {
#         "title": "MAG alarms for section",
#         "device": pyalarm_device
#     }

# WAT Flow gauges (FGE)
# There are two per achromat, one VAC and one MAG related, but they do not
# have a physical position inside the achromat since they are actually
# outside the ring :P Putting them in L for now.
for ach in range(1, 21):
    achromat = "3%02d" % ach
    fge_device = "R3-%s/WAT/FGE-" % achromat
    config["grid"]["cells"]["WAT,%s" % achromat]["grid"]["cells"]["FGE,L"] = {
        "grid": {
            "column_names": ["FGE"],
            "row_names": ["01", "02"],
            "cells": {
                "FGE,%02d" % i: {
                    "device": fge_device + ("%02d" % i)
                } for i in [1, 2]
            }
        }
    }

# WAT
config["grid"]["cells"]["WAT,PLC"] = {"device": "R3/WAT/PLC-01"}

# Shuntgroups
# These are confusing since the devices are named after the room they are physically
# installed in, not the achromat they are connected to. Fortunately there are only six.
config["grid"]["cells"]["WAT,301"]["grid"]["cells"]["SHG,S2"] = {"device": "R3-A100111/WAT/SHG-02"}
config["grid"]["cells"]["WAT,316"]["grid"]["cells"]["SHG,S2"] = {"device": "R3-A101711/WAT/SHG-01"}
config["grid"]["cells"]["WAT,317"]["grid"]["cells"]["SHG,S2"] = {"device": "R3-A101711/WAT/SHG-02"}
config["grid"]["cells"]["WAT,318"]["grid"]["cells"]["SHG,S2"] = {"device": "R3-A101911/WAT/SHG-01"}
config["grid"]["cells"]["WAT,319"]["grid"]["cells"]["SHG,S2"] = {"device": "R3-A101911/WAT/SHG-02"}
config["grid"]["cells"]["WAT,314"]["grid"]["cells"]["SHG,S2"] = {"device": "R3-A111412/WAT/SHG-02"}
config["grid"]["cells"]["WAT,315"]["grid"]["cells"]["SHG,S2"] = {"device": "R3-A111512/WAT/SHG-01"}

# Beamline FE
config["grid"]["cells"]["VAC<br>FE,303"]["grid"]["cells"]["FE,L"] =\
    {"device": "B303A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,304"]["grid"]["cells"]["FE,L"] =\
    {"device": "B304A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,308"]["grid"]["cells"]["FE,L"] =\
    {"device": "B308A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,309"]["grid"]["cells"]["FE,L"] =\
    {"device": "B309A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,310"]["grid"]["cells"]["FE,L"] =\
    {"device": "B310A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,311"]["grid"]["cells"]["FE,L"] =\
    {"device": "B311A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,312"]["grid"]["cells"]["FE,L"] =\
    {"device": "B312A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,316"]["grid"]["cells"]["FE,L"] =\
    {"device": "B316A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,317"]["grid"]["cells"]["FE,L"] =\
    {"device": "B317A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,318"]["grid"]["cells"]["FE,L"] =\
    {"device": "B318A/VAC/FE-01"}

# Beamline VGFA
config["grid"]["cells"]["VAC<br>VGFA,303"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-nanomax-csproxy-0:10000/b303a-fe/vac/vgfa-01".upper()}
config["grid"]["cells"]["VAC<br>VGFA,304"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-danmax-csproxy-0:10000/b304a-fe/vac/vgfa-01".upper()}
config["grid"]["cells"]["VAC<br>VGFA,308"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-balder-csproxy-0:10000/b308a-fe/vac/vgfa-01".upper()}
config["grid"]["cells"]["VAC<br>VGFA,309"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-formax-csproxy-0:10000/b309a-fe/vac/vgfa-01".upper()}
config["grid"]["cells"]["VAC<br>VGFA,310"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-cosaxs-csproxy-0:10000/b310a-fe/vac/vgfa-01".upper()}
config["grid"]["cells"]["VAC<br>VGFA,311"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-biomax-csproxy-0:10000/b311a-fe/vac/vgfa-01".upper()}
config["grid"]["cells"]["VAC<br>VGFA,312"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-micromax-csproxy-0:10000/b312a-fe/vac/vgfa-01".upper()}
config["grid"]["cells"]["VAC<br>VGFA,316"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-veritas-csproxy-0:10000/b316a-fe/vac/vgfa-01".upper()}
config["grid"]["cells"]["VAC<br>VGFA,317"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-hippie-csproxy-0:10000/b317a-fe/vac/vgfa-01".upper()}
config["grid"]["cells"]["VAC<br>VGFA,318"]["grid"]["cells"]["VGFA,L"] =\
    {"device": "tango://b-v-softimax-csproxy-0:10000/b318a-fe/vac/vgfa-01".upper()}

# RF
config["grid"]["cells"]["RF,301"]["grid"] = {
    "column_names": ["NUTAQ", "NUTAQDIAGS", "TXA"],
    "row_names": ["01"],
    "cells": {
        "NUTAQ,01": {"device": "R3-A100111CAB03/RF/NUTAQ-01"},
        "NUTAQDIAGS,01": {"device": "R3-A100111CAB03/RF/NUTAQDIAGS-01"},
        "TXA,01":{"device":"R3-A100111/RF/TXA-51"},

    }
}
config["grid"]["cells"]["RF,316"]["grid"] = {
    "column_names": ["NUTAQ", "NUTAQDIAGS", "TXA"],
    "row_names": ["01"],
    "cells": {
        "TXA,01":{"device":"R3-A101711/RF/TXA-01"},
    }
}
config["grid"]["cells"]["RF,317"]["grid"] = {
    "column_names": ["NUTAQ", "NUTAQDIAGS", "TXA"],
    "row_names": ["01"],
    "cells": {
        "NUTAQ,01": {"device": "R3-A101711CAB03/RF/NUTAQ-01"},
        "NUTAQDIAGS,01": {"device": "R3-A101711CAB03/RF/NUTAQDIAGS-01"},
        "TXA,01":{"device":"R3-A101711/RF/TXA-51"},
    }
}
config["grid"]["cells"]["RF,318"]["grid"] = {
    "column_names": ["NUTAQ", "NUTAQDIAGS", "TXA"],
    "row_names": ["01"],
    "cells": {
        "TXA,01":{"device":"R3-A101911/RF/TXA-01"},

    }
}
config["grid"]["cells"]["RF,319"]["grid"] = {
    "column_names": ["NUTAQ", "NUTAQDIAGS", "TXA"],
    "row_names": ["01"],
    "cells": {
        "NUTAQ,01": {"device": "R3-A101911CAB03/RF/NUTAQ-01"},
        "NUTAQDIAGS,01": {"device": "R3-A101911CAB03/RF/NUTAQDIAGS-01"},
        "TXA,01":{"device":"R3-A101911/RF/TXA-51"},
    }
}
config["grid"]["cells"]["RF,320"]["grid"] = {
    "column_names": ["NUTAQ", "NUTAQDIAGS", "TXA"],
    "row_names": ["01"],
    "cells": {
        "TXA,01":{"device":"R3-A100111/RF/TXA-01"},
    }
}

# DIA (diagnostic) inserts
# Note: these device names are completely wrong and will be changed at some point.
config["grid"]["cells"]["DIA<br>Inserts,301"]["grid"] = {
    "column_names": ["SCRNM", "SCRP"],
    "row_names": ["01", "02", "03"],
    "cells": {
        "SCRNM,01": {"device": "R3-301L/DIA/SCRNM-01-H"},
        "SCRP,01": {"device": "R3-301L/VAC/SCRP-01-H"},
        "SCRP,02": {"device": "R3-301L/VAC/SCRP-02-V"},
        "SCRP,03": {"device": "R3-301L/VAC/SCRP-03-V"},
    }
}


config["grid"]["cells"]["DIA<br>Inserts,311"]["grid"] = {
    "column_names": ["SCRNM", "SCRP"],
    "row_names": ["01"],
    "cells": {
        "SCRP,01": {"device": "R3-311S2/VAC/SCRP-01-H"},
    }
}

# LiberaManager
config["grid"]["cells"]["DIA<br>BPM,Global"] = {"device": "R3/DIA/BPM-MAN"}

#Libera BPM State
for ach in range(1,21):
    achromat = "%02d" % ach
    bpm_state_device = "R3-3{0}/DIA/BPM-STATE-01".format(achromat)
    config["grid"]["cells"]["DIA<br>BPM,3%s" % achromat]["grid"]["cells"]["BPM,"] = {"device": bpm_state_device}

# Libera GDX
for ach in range(1,21):
    achromat = "%02d" % ach
    gdx_device = "R3-3%s/CTL/GDX-" % achromat

    config["grid"]["cells"]["CTL<br>FOFB,3%s" % achromat]["grid"] = {
        "column_names": ["GDX"],
        "row_names": ["01", "02", "03", "State"],
        "cells": {
            "GDX,01" : {
                "device": gdx_device + ("01")
            },
            "GDX,02" : {
                "device": gdx_device + ("02")
            },
            "GDX,03" : {
                "device": gdx_device + ("03")
            },
            "GDX,State": {
                "device": gdx_device + ("STATE-01")
            }
        }
    }

# Libera EVRX
for ach in range(1,21):
    achromat = "%02d" % ach
    evrx_device = "R3-A11%s11/TIM/EVRX-" % achromat

    config["grid"]["cells"]["TIM,3%s" % achromat]["grid"] = {
        "column_names": ["EVR", "EVRX"],
        "row_names": ["01", "02", "03", "State"],
        "cells": {
            "EVRX,01" : {
                "device": evrx_device + ("01")
            },
            "EVRX,02" : {
                "device": evrx_device + ("02")
            },
            "EVRX,03" : {
                "device": evrx_device + ("03")
            },
            "EVRX,State": {
                "device": evrx_device + ("STATE-01")
            }
        }
    }

# OTHER TIM Devices
# EventGenerator
config["grid"]["cells"]["TIM,301"]["grid"]["cells"]["EVR,01"] = {"device": "R3-A110111CAB04/TIM/EVR-01"}
config["grid"]["cells"]["TIM,301"]["grid"]["cells"]["EVR,02"] = {"device": "R3-A110111CAB04/TIM/EVR-01-L"}
config["grid"]["cells"]["TIM,308"]["grid"]["cells"]["EVR,01"] = {"device": "R3-A110811CAB04/TIM/EVR-01"}
config["grid"]["cells"]["TIM,310"]["grid"]["cells"]["EVR,01"] = {"device": "R3-A111011CAB04/TIM/EVR-01"}
config["grid"]["cells"]["TIM,319"]["grid"]["cells"]["EVR,01"] = {"device": "R3-A101911CAB03/TIM/EVR-01-MASTER"}

# TIM Global
config["grid"]["cells"]["TIM,Global"] = {
    "grid": {
        "column_names": ["EVG", "EVR", "MO", "SEL"],
        "row_names": ["01"],
        "cells": {
            "EVG,01": {"device": "R3-A101911CAB03/TIM/EVG-01"},
            # EventGenerator
            "EVR,01": {"device": "I-K00/TIM/EVR-01-R3"},
            # RFSignalGenerator, "master oscillator"
            "MO,01": {"device": "R3-A101911/RF/MO-01"},
            # InjectionSelector
            "SEL,01": {"device": "G/TIM/SEL"}  # maybe should go in an even more "global place"?
        }
    }
}

# DCCT and RTO
config["grid"]["cells"]["DIA,319"] = {
    "grid": {
        "column_names": ["DCCT", "RTO"],
        "row_names": ["01"],
        "cells": {
            "DCCT,01": {"device": "R3-319S2/DIA/DCCT-01"},
            "RTO,01": {"device": "R3-A111911/DIA/OSCA-01"}
        }
    }
}


# Cameras and RTO
config["grid"]["cells"]["DIA,301"] = {
    "grid": {
        "column_names": ["SRMON", "SCRNM", "RTO"],
        "row_names": ["01"],
        "cells": {
            "SRMON,01": {"device": "R3-301L/DIA/CAM-01"},
            "SCRNM,01": {"device": "R3-301L/DIA/CAM-01"},
            "RTO,01": {"device": "R3-A110111/DIA/OSCA-01"}
        }
    }
}

# Spectrum analyzer
config["grid"]["cells"]["DIA,320"] = {"device": "R3-A112011CAB04/DIA/SPA-01"}


# insertion devices - as we add more, will need to make this less manual!
for ach in [3, 4, 8, 9, 10, 11, 12, 16, 17, 18]:
    achromat = "3%02d" % ach
    locker_device = "R3-%sL/ID/LOCKER-01" % achromat
    config["grid"]["cells"]["ID,%s" % achromat]["grid"] = {
        "column_names": ["LOCKER"],
        "row_names": ["01"],
        "cells": {
            "LOCKER,01" : {
                "device": locker_device
            }
        }
    }

config["grid"]["cells"]["CTL,303"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-303/MAG/IVUFB-01"},
        }
    }
}
config["grid"]["cells"]["CTL,304"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-304/MAG/IVUFB-01"},
        }
    }
}
config["grid"]["cells"]["CTL,308"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-308/MAG/IVUFB-01"},
        }
    }
}
config["grid"]["cells"]["CTL,309"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-309/MAG/IVUFB-01"},
        }
    }
}
config["grid"]["cells"]["CTL,310"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-310/MAG/IVUFB-01"},
        }
    }
}
config["grid"]["cells"]["CTL,311"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-311/MAG/IVUFB-01"},
        }
    }
}
config["grid"]["cells"]["CTL,312"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-312/MAG/IVUFB-01"},
        }
    }
}
config["grid"]["cells"]["CTL,316"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-316/MAG/EPUFB-01"},
        }
    }
}
config["grid"]["cells"]["CTL,317"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-317/MAG/EPUFB-01"},
        }
    }
}


config["grid"]["cells"]["CTL,318"] = {
    "grid": {
        "column_names": ["FF"],
        "row_names": ["01"],
        "cells":{
            "FF,01": {"device":"R3-318/MAG/EPUFB-01"},
        }
    }
}

# XBPMs
for ach in [3, 4, 8, 9, 10, 11, 12, 16, 17, 18]:
    achromat = "3%02d" % ach
    xbpm_device = "B%sA/DIA/XBPM-" % achromat
    config["grid"]["cells"]["DIA,%s" % achromat]["grid"] = {
        "column_names": ["XBPM"],
        "row_names": ["01", "02"],
        "cells": {
            "XBPM,01" : {
                "device": xbpm_device + "01"
            },
            "XBPM,02" : {
                "device": xbpm_device + "02"
            }
        }
    }

# COID
config["grid"]["cells"]["MAG,303"]["grid"]["cells"]["COID,L"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R3-A110310CAB11/MAG/PSIA-03"},
            "FF01,02": {"device": "R3-A110310CAB11/MAG/PSIA-04"},
            "FF01,03": {"device": "R3-A110310CAB11/MAG/PSIA-05"},
            "FF01,04": {"device": "R3-A110310CAB11/MAG/PSIA-06"},
        }
    }
}
config["grid"]["cells"]["MAG,304"]["grid"]["cells"]["COID,L"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R3-A110410CAB12/MAG/PSIA-01"},
            "FF01,02": {"device": "R3-A110410CAB12/MAG/PSIA-02"},
            "FF01,03": {"device": "R3-A110410CAB12/MAG/PSIA-03"},
            "FF01,04": {"device": "R3-A110410CAB12/MAG/PSIA-04"},
        }
    }
}
config["grid"]["cells"]["MAG,309"]["grid"]["cells"]["COID,L"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R3-A110910CAB12/MAG/PSIA-01"},
            "FF01,02": {"device": "R3-A110910CAB12/MAG/PSIA-02"},
            "FF01,03": {"device": "R3-A110910CAB12/MAG/PSIA-03"},
            "FF01,04": {"device": "R3-A110910CAB12/MAG/PSIA-04"},
        }
    }
}
config["grid"]["cells"]["MAG,310"]["grid"]["cells"]["COID,L"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R3-A111010CAB12/MAG/PSIA-01"},
            "FF01,02": {"device": "R3-A111010CAB12/MAG/PSIA-02"},
            "FF01,03": {"device": "R3-A111010CAB12/MAG/PSIA-03"},
            "FF01,04": {"device": "R3-A111010CAB12/MAG/PSIA-04"},
        }
    }
}
config["grid"]["cells"]["MAG,311"]["grid"]["cells"]["COID,L"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R3-A111110CAB11/MAG/PSIA-03"},
            "FF01,02": {"device": "R3-A111110CAB11/MAG/PSIA-04"},
            "FF01,03": {"device": "R3-A111110CAB11/MAG/PSIA-05"},
            "FF01,04": {"device": "R3-A111110CAB11/MAG/PSIA-06"},
        }
    }
}
config["grid"]["cells"]["MAG,312"]["grid"]["cells"]["COID,L"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R3-A111210CAB12/MAG/PSIA-01"},
            "FF01,02": {"device": "R3-A111210CAB12/MAG/PSIA-02"},
            "FF01,03": {"device": "R3-A111210CAB12/MAG/PSIA-03"},
            "FF01,04": {"device": "R3-A111210CAB12/MAG/PSIA-04"},
        }
    }
}
config["grid"]["cells"]["MAG,316"]["grid"]["cells"]["COID,L"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R3-A111610CAB13/MAG/BN074-01"},
            "FF01,02": {"device": "R3-A111610CAB13/MAG/BN074-02"},
            "FF01,03": {"device": "R3-A111610CAB13/MAG/BN074-03"},
            "FF01,04": {"device": "R3-A111610CAB13/MAG/BN074-04"},
        }
    }
}
config["grid"]["cells"]["MAG,317"]["grid"]["cells"]["COID,L"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R3-A111710CAB13/MAG/PSIA-01"},
            "FF01,02": {"device": "R3-A111710CAB13/MAG/PSIA-04"},
            "FF01,03": {"device": "R3-A111710CAB13/MAG/PSIA-07"},
            "FF01,04": {"device": "R3-A111710CAB13/MAG/PSIA-10"},
        }
    }
}
config["grid"]["cells"]["MAG,318"]["grid"]["cells"]["COID,L"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R3-A111810CAB12/MAG/PSIA-01"},
            "FF01,02": {"device": "R3-A111810CAB12/MAG/PSIA-02"},
            "FF01,03": {"device": "R3-A111810CAB12/MAG/PSIA-03"},
            "FF01,04": {"device": "R3-A111810CAB12/MAG/PSIA-04"},
        }
    }
}
print(json.dumps(config, indent=4))
