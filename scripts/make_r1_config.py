"""
This script generates a JSON stategrid config for the 1.5GeV storage
ring at MAXIV. The output should be redirected to a file that can then
be used as argument to the stategrid application.
"""

from itertools import chain, product
import json
import re
import sys

#import tango

# These files are expected to be JSON format with device names as keys
# and anything as values (those are ignored). They should contain all
# devices that should be in the grid.
# They are identical to the files used by the r1synoptic, generated
# from various XLS files.

devs = {}

with open("r1_mag.json") as f:
    magnets = json.load(f)
    devs.update(magnets)

with open("r1_vac.json") as f:
    vacs = json.load(f)
    devs.update(vacs)

#with open("r1_bpm.json") as f:
#    bpms = json.load(f)
#    # for bpm in bpms.keys():
#    #     bpms[bpm.replace("DIA", "TIM")] = {}
#    devs.update(bpms)

with open("r1_dia.json") as f:
    devs.update(json.load(f))

with open("r1_wat.json") as f:
    devs.update(json.load(f))

with open("r1_alarms.json") as f:
    pyalarms = json.load(f)


# # ignore some devices (e.g. fast corrector magnets)
#devices_to_skip = [re.compile(p, flags=re.IGNORECASE)
#                   for p in [
#                           "^R3-3.*\/MAG\/COG.*$",  # fast correctors, not implemented
                           # "^R3-3\/VAC\/VGMB-01"  # valves that are in beamlines
                           #                          # maybe need to be shown, but somehow ignored?
#                   ]]


#for regex in devices_to_skip:
#    for device in devs.keys():
#        if regex.match(device):
#            del devs[device]

# skip VGMB that does not belong to FE (added manually below)
for device in [
        "R1-101S/VAC/VGMB-01"]:
    del devs[device]
#

# states in order of increasing importance
state_prios = ["N/A", "RUNNING", "ON", "OPEN", "EXTRACT",
               "STANDBY", "OFF", "CLOSE", "INSERT", "MOVING", "DISABLE",
               "INIT", "ALARM", "FAULT", "UNKNOWN"]

# These are used to name columns and rows in the various grid levels
# 'None' indicates a separator
subsystems = ["VAC<br>R Pumps", "VAC<br>R Valves", "VAC<br>FE Pumps", "VAC<br>FE Inserts", "VAC<br>FE", "VAC<br>VGFA",
              None, "WAT", None, "MAG", None, "RF", None,
              "TIM", None,
              "DIA", "DIA<br>TCO", "DIA<br>BPM", "DIA<br>Inserts", "CTL<br>FOFB", "CTL",  None, "ID", None, "MPS", "PSS"]

# The ring consists of 12 almost identical sections called achromats
achromats = ["101", "102", "103", "104", None,
             "105", "106", "107", "108", None,
             "109", "110", "111", "112", None,
             # I guess these things could be handled better...
             "PLC", None, "Alarm", None, "Global"]

# in each achromat is a set of "cells" (this is not the right terminology)
#cells = ["L", None, "M1", "S1", None, "U1", "U2", "U3", "U4", "U5", None, "S2", "M2"]
cells = ["", None, "S"]

# The different types of devices sorted by subsystem
device_types = {
    "MAG": [# "ALARM", None,
#        "COFY",
#        None,
        "DIP", "DIPC", None,
        "PMH1", "PMV1", None,
        "SQFI", "SQFO", None,
        "SXCI", "SXCO", None,
        "SXDI", "SXDO", None,
        "COID"
    ],
    "VAC<br>R Pumps": [
        "IPFA", "IPFB", "IPFC", "IPFE", None,"IPGA", "IPGB", None, "IPH", "IPK", "IPN"],
         #"IPFA", "IPFB" "IPFC", None, "IPGA", "IPGB", None, "IPH", "IPK", "IPN", "IPR", None, "VGC", "VGHE"] r3 order
    "VAC<br>R Valves": ["VGRC"],
    #FIX to avoid column collision for the VGMB valves (one valve needs to be in R instead of FE)
    #"VAC<br>FE Pumps": ["IPFE"], added explicitly below
    "VAC<br>FE Inserts": ["VGMB", "VGMC", None, "HAA", "HAB"],
    "DIA": ["DCCT"],
    "DIA<br>TCO": ["TCO"],  # Other sensors?
    "DIA<br>BPM": ["BPM"],
    "DIA<br>Inserts": ["SCRN", "SCRNM"],
    "WAT": ["FGE", "FSW", "SHG", "TSE"],
    "RF": ["NUTAQ", "NUTAQDIAGS", "TXA"],
    "TIM": ["EVR", "EVRX", None, "FOUT"],
    "PSS":["PSS"],
    "MPS":["FE", None, "VAC", "MAG", "ID"],
    # "TIM/BPM": ["BPM"],
    "ID": ["LOCKER"]
}

titles = {
    "SHG": "Shuntgroup",
    "FGE": "Flow gauge",
    "FSW": "Flow switch"
}

# # Not used anymore as we read all the info from JSON files above instead of
# # the Tango DB, keeping this for future reference :)
# db = tango.Database()
# devices = {
#     "MAG": list(m for m in db.get_device_exported_for_class("Magnet")
#                 if m.upper().startswith("R3-")),
#     "VAC": (list(db.get_device_exported("R3-*/VAC/IP*")) +
#             list(db.get_device_exported("R3-*/VAC/VGC-*")) +
#             list(db.get_device_exported("R3-*/VAC/VGHE-*"))),
#     "VAC/VG": list(v for v in db.get_device_exported_for_class("VacuumValve")
#                    if v.upper().startswith("R3-")),
#     "DIA": (list(db.get_device_exported("R3-*/DIA/TCO-*")) +
#     "DIA/Cam": list(db.get_device_exported("R3-*/DIA/BPM-*")),
#     "DIA/Inserts": list(db.get_device_exported("R3-*/DIA/SCRN*"))
# }


_devpatt = {}
_devcache = {}
_devreqs = {}  # just for displaying progress


def get_devices(*args):
    if args not in _devpatt:
        achromat, cell, subsystem, devstart = args
        subs = re.split("/|<br>", subsystem)[0]
        pattern = re.compile(
            f"R1-{achromat}{cell}/{subs}/{devstart}-[0-9][0-9]$",
            re.IGNORECASE
        )
        _devpatt[args] = pattern
    else:
        pattern = _devpatt[args]
    if pattern not in _devcache:
        matches = [dev for dev in devs if pattern.search(dev)]
        for device in matches:
            if device in magnets:
                data = magnets[device]
                # Add related skew/corrector coil circuits
                circuits = data.get("circuits")
                if circuits:
                    matches.extend(circuits)

        _devcache[pattern] = matches
        print("\r%d       " % len(_devcache), end=' ', file=sys.stderr)
    else:
        _devreqs[pattern] = _devreqs.setdefault(args, 1) + 1
    return _devcache[pattern]


def get_device(*args):
    devices = get_devices(*args)
    if len(devices) == 1:
        return devices[0]


def get_snumber(*args):
    dev = get_device(*args)
    if dev:
        return devs[dev].get("s", 0)


def devnr(devicename):
    "get the instance number from a device name"
    pattern = re.compile(r'BPM-EVRX|BPM|GDX$')
    if pattern.match(devicename):
        return int(devicename.split("-")[-1])
    else:
        return int(devicename.split("/")[-1].split("-")[1])


def unique(seq, idfun=None):
    # order preserving uniquify list
    if idfun is None:
        def idfun(x):
            return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        if marker in seen:
            continue
        seen[marker] = 1
        result.append(item)
    return result


def devtype_seq(ach, cell, sub, devtype):
    dev_seq = []
    for device in get_devices(ach, cell, sub, devtype):
        name_parts = device.split("/")[-1].split("-")
        if len(name_parts) > 2:
            devname = "{}-{}".format(name_parts[0],name_parts[2])
        else:
            devname = name_parts[0]
        dev_seq.append(devname)
    return dev_seq


def get_cell(device, achromat=None):
    device_parts = device.split("/")
    name_parts = device.split("/")[-1].split("-")
    loc = device_parts[0].split("-")[1]
    if len(name_parts) > 2:
        cell = "{},{}".format("{}-{}".format(name_parts[0], name_parts[2]), name_parts[1])
    elif loc == "1":
        cell = "{},{}".format(name_parts[0], "Global")
    elif achromat and achromat != loc:
        # Things that actually belong in other parts (circuits...)
        cell = "{},{}".format(name_parts[0], device_parts[0].split("-")[1])
    else:
        cell = "{},{}".format(*name_parts)
    return cell


def get_row_names(ach, cell, sub, devtype):
    row_names = []
    pre_rows = []
    post_rows = []
    global_row = False
    for device in get_devices(ach, cell, sub, devtype):
        row_names.append(devnr(device))
        loc = device.split("/")[0].split("-")[1]
        if loc == "1":
            # Put global devices (common to all R1) in their own row
            global_row = True
        elif ach != loc:
            # Things from other locations also get their own rows
            # Probably does not work too well if there are several...
            if loc[:3] <= ach:
                pre_rows.append(loc)
            else:
                post_rows.append(loc)

    row_names = ["%02d" % (x+1) for x in range(max(row_names))]
    if pre_rows:
        row_names = pre_rows + [None] + row_names
    if post_rows:
        row_names = row_names + [None] + post_rows
    if global_row:
        row_names += [None, "Global"]
    return row_names


# build the config in one go, using dict comprehensions
config = {

    # This is the main grid
    "title": "1.5 GeV ring",
    "state_device": "R1/CTL/STATE",  # toplevel UnitedStates device
    "window_size": {"width": 1200, "height": 800},
    # "state_priorities": state_prios,

    "grid": {
        "row_names": achromats,
        "column_names": subsystems,
        "cells": {
            "{},{}".format(subs, ach): {  # column first (should be swapped?)
                # Intermediate grid, each row showing sections within an achromat
                # and columns being device types within a subsystem
                "title": "R1-{}/{}".format(ach, subs.replace("<br>", "/")),
                "grid": {
                    "column_names": device_types.get(subs, []),
                    "row_names": cells,
                    "cells": {
                        "{},{}".format(devtype, cell): {
                            # Lowest level grid, showing individual devices
                            "device": get_device(ach, cell, subs, devtype),
                            "key": get_snumber(ach, cell, subs, devtype),
                            "title": "R1-{}{}/{}/{}".format(ach, cell, subs.split("/")[0].replace("<br>", "/"), devtype.replace("<br>", "/")),
                            #"state_priorities": state_prios,
                            "grid": {
                                "column_names": unique(
                                    devtype_seq(ach, cell, subs.split("/")[0], devtype)),
                                "row_names": get_row_names(ach, cell, subs, devtype),

                                #["%02d" % (x+1) for x in
                                # xrange(max(devnr(dev) for dev in
                                # get_devices(ach, cell, subs, devtype)))],

                                "cells": {
                                    get_cell(device, ach): {
                                        "title": device,
                                        "device": device,
                                        # "key": devs[device].get("s", 0)  # not all devices have s...
                                    }
                                    for device in get_devices(ach, cell, subs, devtype)
                                }
                            } if len(get_devices(ach, cell, subs, devtype)) > 1 else None
                        }
                        for devtype, cell in product(device_types.get(subs, []), cells)
                        if get_devices(ach, cell, subs, devtype)
                    }
                }
            } for ach, subs in product(achromats, subsystems) if ach and subs
        }
    }
}

# Add stuff that isn't in the input files, or doesn't fit the "standard"


# Add the skipped VGMB in R Valves (see above)
PATTERN = re.compile("R1-1(\d\d)(.*)/.*/(.*)-\d\d")
for device in [
        "R1-101S/VAC/VGMB-01"]:
    parts = PATTERN.match(device)
    achromat, cell, devtype = parts.groups()
    if "VAC<br>R Valves,1%s" % achromat not in config["grid"]["cells"]:
        config["grid"]["cells"]["VAC<br>R Valves,1%s" % achromat] = {"grid": {"cells": {}}}
    config["grid"]["cells"]["VAC<br>R Valves,1%s" % achromat]["grid"]["column_names"] = ["VGMB ", None, "VGRC"]
    config["grid"]["cells"]["VAC<br>R Valves,1%s" % achromat]["grid"]["cells"]["%s,%s" % ("VGMB ", cell)] = {
        "device": device
    }
#Workaround for a bug that makes some valves hidden
# TODO: Clean this workaround solution
for i in range(2,10) :
    config["grid"]["cells"]["VAC<br>R Valves,10{}".format(i)]["grid"]["cells"]["%s,%s" % ("VGMB ", "S")] = {}

for i in range(10,13) :
    config["grid"]["cells"]["VAC<br>R Valves,1{}".format(i)]["grid"]["cells"]["%s,%s" % ("VGMB ", "S")] = {}

#Add frontend pumps
config["grid"]["cells"]["VAC<br>FE Pumps,105"] = {
    "title": "B105/VAC/FE Pumps",
    "grid": {
        "column_names": ["IPFC"],
        "row_names": ["A", "B"],
        "cells": {
            "IPFC,A": {
            "title": "B105A-O/VAC/IPFC",
            "grid": {
                "column_names": ["IPFC"],
                "row_names": ["1", "2"],
                "cells": {
                    "IPFC,1": {"device": "B105A-O/VAC/IPFC-01"},
                    "IPFC,2": {"device": "B105A-O/VAC/IPFC-02"}
                }
            }
            },
            "IPFC,B": {
            "title": "B105B-O/VAC/IPFC",
            "grid": {
                "column_names": ["IPFC"],
                "row_names": ["1", "2"],
                "cells": {
                    "IPFC,1": {"device": "B105B-O/VAC/IPFC-01"},
                    "IPFC,2": {"device": "B105B-O/VAC/IPFC-02"}
                }
            }
            }
        }
    }
}


# "Pulsed" magnets
config["grid"]["cells"]["MAG,103"]["grid"]["cells"]["PMH1,S"] = {
    "title": "MAG Kicker",
    "grid": {
        "column_names": ["PMH1", "CRPMH1"],
        "row_names": ["01"],
        "cells": {
            "PMH1,01": {"device": "R1-103S/MAG/PMH1-01"},
            "CRPMH1,01": {"device": "R1-103S/MAG/CRPMH1-01"}
        }
    }
}
config["grid"]["cells"]["MAG,101"]["grid"]["cells"]["PMV1,S"] = {
    "title": "MAG Pinger",
    "grid": {
        "column_names": ["PMV1", "CRPMV1"],
        "row_names": ["01"],
        "cells": {
            "PMV1,01": {"device": "R1-101S/MAG/PMV1-01"},
            "CRPMV1,01": {"device": "R1-101S/MAG/CRPMV1-01"}
        }
    }
}


# VAC PLCs
achromats = ["%02d" % a for a in range(1, 13)]
vac_plcs = ["PLC-01"]
config["grid"]["cells"]["VAC<br>R Valves,PLC"] = {
    "title": "VAC PLC",
    "grid": {
        "column_names": vac_plcs,
        "row_names": achromats,
        "cells": {
            "{},{}".format(plc, ach): {"device": "R1-1%s/VAC/%s" % (ach, plc)}
            for plc, ach in product(vac_plcs, achromats)
        }
    }
}

# MAG PLCs
magnet_plc_systems = ["MAG"] #["MAG", "SWB"]
config["grid"]["cells"]["MAG,PLC"] = {
    "title": "MAG PLC",
    "grid": {
        "column_names": magnet_plc_systems,
        "row_names": achromats,
        "cells": {
            "{},{}".format(system, ach): {
                "device": "R1-1%s/MAG/PLC-01" % ach #if system == "MAG"
              #  else "R3-A11%s11/MAG/PLC-02" % ach
            } for system, ach in product(magnet_plc_systems, achromats)
        }
    }
}

# MPS

# PS-MAG-MONITOR PyAlarm (monitors temperature changes through resistance)
#config["grid"]["cells"]["MAG,Alarm"] = {
#    "title": "MAG Alarm",
#    "device": "R3/MAG/PS-ALARM"
#}

# MAG PyAlarm
#for ach in range(1, 13):
#    achromat = "1%02d" % ach
#    pyalarm_device = "R1-%s/MAG/ALARM-01" % achromat
#    config["grid"]["cells"]["MAG,%s" % achromat]["grid"]["cells"]["ALARM,S"] = {
#         "title": "MAG alarms for section",
#         "device": pyalarm_device
#    }

### # WAT Flow gauges (FGE)
### # There are two per achromat, one VAC and one MAG related
### for ach in range(1, 13):
###     achromat = "1%02d" % ach
###     fge_device = "R1-%s/WAT/FGE-" % achromat
###     config["grid"]["cells"]["WAT,%s" % achromat]["grid"]["cells"]["FGE,"] = {
###         "title": "R1-%s/WAT/FGE" % achromat,
###         "grid": {
###             "column_names": ["FGE"],
###             "row_names": ["01", "02"],
###             "cells": {
###                 "FGE,%02d" % i: {
###                     "device": fge_device + ("%02d" % i)
###                 } for i in [1, 2]
###             }
###         }
###     }
###
### # WAT Flow switches (FSW)
### # There are two per achromat, one VAC and one MAG related
### for ach in range(1, 13):
###     achromat = "1%02d" % ach
###     fsw_device = "R1-%s/WAT/FSW-" % achromat
###     config["grid"]["cells"]["WAT,%s" % achromat]["grid"]["cells"]["FSW,"] = {
###         "title": "R1-%s/WAT/FSW" % achromat,
###         "grid": {
###             "column_names": ["FSW"],
###             "row_names": ["01", "02"],
###             "cells": {
###                 "FSW,%02d" % i: {
###                     "device": fsw_device + ("%02d" % i)
###                 } for i in [1, 2]
###             }
###         }
###     }
###
###
# WAT
config["grid"]["cells"]["WAT,PLC"]["grid"]["cells"]["PLC,"] = {"device": "R1/WAT/PLC-01"}

# Shuntgroups (SHG)
# These are confusing since the devices are named after the room they are physically
# installed in, not the achromat they are connected to.
# There are four in achromat 5
shg_device = "R1-D110410/WAT/SHG-"
config["grid"]["cells"]["WAT,105"]["grid"]["cells"]["SHG,"] = {
    "title": "R1-105/DIA/SHG",
    "grid": {
        "column_names": ["SHG"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "SHG,%02d" % i: {
                "device": shg_device + ("%02d" % i)
            } for i in [1, 2, 3, 4]
        }
    }
}

# RF
# Nutaq
config["grid"]["cells"]["RF,105"]["grid"] = {
    "column_names": ["NUTAQ", "NUTAQDIAGS", "TXA"],
    "row_names": ["01", "02"],
    "cells": {
        "NUTAQ,01": {"device": "R1-D100101CAB03/RF/NUTAQ-01"},
        "NUTAQDIAGS,01": {"device": "R1-D100101CAB03/RF/NUTAQDIAGS-01"},
        "TXA,01": {"device":"r1-d100101/rf/txa-01".upper()},
        "TXA,02": {"device":"r1-d100101/rf/txa-02".upper()}
    }
}

# DIA (diagnostic) inserts
config["grid"]["cells"]["DIA<br>Inserts,101"]["grid"] = {
    "column_names": ["SCRNM", "SCRP"],
    "row_names": ["01", "02", "03"],
    "cells": {
        "SCRNM,01": {"device": "R1-101S/DIA/SCRNM-01-H"},
        "SCRP,01": {"device": "R1-101S/VAC/SCRP-01-V"},
        "SCRP,02": {"device": "R1-101S/VAC/SCRP-02-V"},
        "SCRP,03": {"device": "R1-101S/VAC/SCRP-03-H"},
    }
}


# # TIM (timing)

# FanoutConcentrator
# Note: removed by request from Jerzy

# fconc = [
#     'R3-A110111CAB04/TIM/FOUT-31', 'R3-A110211CAB04/TIM/FOUT-21', 'R3-A110211CAB04/TIM/FOUT-31',
#     'R3-A110311CAB04/TIM/FOUT-31', 'R3-A110411CAB04/TIM/FOUT-31', 'R3-A110511CAB04/TIM/FOUT-31',
#     'R3-A110611CAB04/TIM/FOUT-31', 'R3-A110711CAB04/TIM/FOUT-21', 'R3-A110711CAB04/TIM/FOUT-31',
#     'R3-A110811CAB04/TIM/FOUT-31', 'R3-A110911CAB04/TIM/FOUT-31', 'R3-A111011CAB04/TIM/FOUT-31',
#     'R3-A111111CAB04/TIM/FOUT-31', 'R3-A111211CAB04/TIM/FOUT-21', 'R3-A111211CAB04/TIM/FOUT-31',
#     'R3-A111311CAB04/TIM/FOUT-31', 'R3-A111411CAB04/TIM/FOUT-31', 'R3-A111511CAB04/TIM/FOUT-31',
#     'R3-A111611CAB04/TIM/FOUT-31', 'R3-A111711CAB04/TIM/FOUT-21', 'R3-A111711CAB04/TIM/FOUT-31',
#     'R3-A111811CAB04/TIM/FOUT-31', 'R3-A111911CAB04/TIM/FOUT-11', 'R3-A111911CAB04/TIM/FOUT-31',
#     'R3-A112011CAB04/TIM/FOUT-31'
# ]

# for ach in range(1, 21):
#     config["grid"]["cells"]["TIM,3%02d" % ach] = {
#         "grid": {
#             # There are three hierarchical levels of Fconc devices;
#             # L1-3. L1 contains only one, L2 four and L3 20 (one per
#             # achromat? So, 11 means level 1, device 1, 31 level 3, dev 1 and so on.)
#             "column_names": device_types["TIM"],
#             "row_names": ["01", None, "11", "21", "31"],
#             "cells": {
#                 "FOUT,%s" % num: {"device": "R3-A11%02d11CAB04/TIM/FOUT-%s" % (ach, num)}
#                 for num in ["11", "21", "31"]
#                 if "R3-A11%02d11CAB04/TIM/FOUT-%s" % (ach, num) in fconc
#             }
#         }
#     }

# EventGenerator
config["grid"]["cells"]["TIM,102"]["grid"] = {
    "column_names": ["EVR", "EVR-MASTER", "EVRX"],
    "row_names": ["01","02"],
    "cells" : {
        "EVR,01": {"device": "R1-D110210CAB04/TIM/EVR-01"},
        "EVR-MASTER,01": {"device": "R1-D110210CAB04/TIM/EVR-01-MASTER"},
        "EVRX,01": {"device": "R1-D110210/TIM/EVRX-01"},
        "EVRX,02": {"device": "R1-D110210/TIM/EVRX-02"},
    }
}

# Libera Event Receivers (EVRX) - see above for those in achr 2
for ach in range(1,13):
    achromat = "1%02d" % ach
    evrx_device = "R1-%s/TIM/EVRX-" % achromat
    config["grid"]["cells"]["TIM,%s" % achromat]["grid"] = {
        "column_names": ["EVRX"],
        "row_names": ["01", "State"],
        "cells": {
            "EVRX,01" : {
                "device": evrx_device + ("01")
            },
            "EVRX,State": {
                "device": evrx_device + ("STATE-01")
            }

        }
    }



# TIM Global
config["grid"]["cells"]["TIM,Global"]["grid"] = {
    "column_names": ["EVG", "EVR", "MO", "SEL"],
    "row_names": ["01"],
    "cells": {
        # EventGenerator
        "EVG,01": {"device": "R1-D110210CAB04/TIM/EVG-01"},
        # EventReceiver
        "EVR,01": {"device": "I-K00/TIM/EVR-01-R1"},
        # RFSignalGenerator, "master oscillator"
        "MO,01": {"device": "R1-D100101/RF/MO-01"},
        # InjectionSelector
        "SEL,01": {"device": "G/TIM/SEL"}  # maybe should go in an even more "global place"?
    }
}


# LiberaManager
config["grid"]["cells"]["DIA<br>BPM,Global"] = {"device": "R1/DIA/BPM-MAN"}

#Libera BPM State
for ach in range(1,13):
    achromat = "%02d" % ach
    bpm_state_device = "R1-1{0}/DIA/BPM-STATE-01".format(achromat)
    config["grid"]["cells"]["DIA<br>BPM,1%s" % achromat]["grid"]["cells"]["BPM,"] = {"device": bpm_state_device}

# Libera GDX
for ach in range(1,13):

    achromat = "%02d" % ach
    gdx_device = "R1-1%s/CTL/GDX-" % achromat
    config["grid"]["cells"]["CTL<br>FOFB,1%s" % achromat]["grid"] = {
        "column_names": ["GDX"],
        "row_names": ["01", "State"],
        "cells": {
            "GDX,01" : {
                "device": gdx_device + ("01")
            },
            "GDX,State": {
                "device": gdx_device + ("STATE-01")
            }
        }
    }

# CTL loops
for ach in [7,8,10,11,12]:
    achromat = "%02d" % ach
    if int(ach) == 7:
        config["grid"]["cells"]["CTL,1%s" % achromat]["grid"] = {
            "column_names": ["FF"],
            "row_names": ["01","02"],
            "cells": {
                "FF,01": {"device":"R1-1{}/MAG/IVUFB-01".format(achromat)},
                "FF,02": {"device":"R1-1{}/CTL/PUFF-02".format(achromat)}
            }
        }

    elif int(ach) == 12:
        config["grid"]["cells"]["CTL,1%s" % achromat]["grid"] = {
            "column_names": ["FF"],
            "row_names": ["01","02","03"],
            "cells": {
                "FF,01": {"device":"R1-1{}/MAG/EPUFB-01".format(achromat)},
                "FF,02": {"device":"R1-1{}/CTL/EPUFF-02".format(achromat)},
                "FF,03": {"device":"R1-1{}/CTL/EPUFF-03".format(achromat)}
            }
        }
    elif int(ach) in [8, 10, 11, 12]:
        config["grid"]["cells"]["CTL,1%s" % achromat]["grid"] = {
            "column_names": ["FF"],
            "row_names": ["01","02"],
            "cells": {
                "FF,01": {"device":"R1-1{}/MAG/EPUFB-01".format(achromat)},
                "FF,02": {"device":"R1-1{}/CTL/EPUFF-02".format(achromat)},
                "FF,State": {"device":None}
            }
        }
    else:
        config["grid"]["cells"]["CTL,1%s" % achromat]["grid"] = {
            "column_names": ["GDX"],
            "row_names": ["01","02", "State"],
            "cells": {
                "FF,01":{"device":None},
                "FF,02":{"device":None},
                "FF,State":{"device":None}
            }
        }

# Scope
config["grid"]["cells"]["DIA,102"] = {"device": "R1-D110210/DIA/OSCA-01"}

# SpectrumAnalyzer
config["grid"]["cells"]["DIA,112"] = {"device": "R1-D111210CAB04/DIA/SPA-01"}

#DCCT
config["grid"]["cells"]["DIA,101"] = {"device": "R1-101S/DIA/DCCT-01"}


# PSS / MPS
alarms_domains = { domain.replace("R1-", "").replace("B", "")[0:3] :  [] for
                  domain in set([device.split("/")[0] for device in pyalarms
                                 if not "pss" in device.split("/")[1].lower()])}



columns = ["FE", None, "VAC", "MAG", "ID"]

rows = set()

for dev in  pyalarms:
    domain, familly, member = dev.split("/")
    if "pss" in familly.lower():
        continue
    try:
        rows.add(member.split("-")[1])
    except IndexError:
        pass
    alarms_domains[domain.replace("R1-", "").replace("B", "")[0:3] ].append(dev)



def get_alarm_column(dev):
    domain, familly, member = dev.split("/")
    if familly == "PSS":
        return familly
    if domain.startswith("B"):
        return "FE"
    else:
        return familly

def get_alarm_row(dev):
    domain, familly, member = dev.split("/")
    try:
        return member.split("-")[1]
    except IndexError:
        return "01"


for domain, devices in alarms_domains.items():
    config["grid"]["cells"]["MPS,{}".format(domain)] = {
        "grid": {
            "column_names": columns,
            "row_names": sorted(list(rows)),
            "cells":{
                "{},{}".format(get_alarm_column(device),
                        get_alarm_row(device)): {
                            "title": device,
                            "device": device,

            } for device in devices
         }
        }
    }

config["grid"]["cells"]["MPS,105"]["grid"]["column_names"] =\
    ["FE", "FE(1)", None, "VAC", "MAG", "ID", "ALARM"]
config["grid"]["cells"]["MPS,105"]["grid"]["cells"]["FE(1),01"] =\
     {"device": "B105B/VAC/ALARM-01"}
config["grid"]["cells"]["MPS,105"]["grid"]["cells"]["ALARM,01"] =\
     {"device": "R1-105/WAT/ALARM-01"}

# PSS
#alarms_domains = { domain.replace("R1-", "").replace("B", "")[0:3] :  [] for
#                  domain in set([device.split("/")[0] for device in pyalarms
#                                 if "pss" in device.split("/")[1].lower()])}


#columns = ["PSS"]

#rows = set()

#for dev in  pyalarms:
#    domain, familly, member = dev.split("/")
#    if not "pss" in familly.lower():
#        continue
#    try:
#        rows.add(member.split("-")[1])
#    except IndexError:
#        pass
#    alarms_domains[domain.replace("R1-", "").replace("B", "")[0:3] ].append(dev)


#for domain, devices in alarms_domains.iteritems():
#    config["grid"]["cells"]["PSS,{}".format(domain)] = {
#        "grid": {
#            "column_names": columns,
#            "row_names": sorted(list(rows)),
#            "cells":{
#                "{},{}".format(get_alarm_column(device),
#                        get_alarm_row(device)): {
#                            "title": device,
#                            "device": device,

#            } for device in devices
#         }
#        }
#    }



# Add global pyalarms
columns = ["VAC", "MAG", "ID", "WAT"]
config["grid"]["cells"]["MPS,Alarm"]["grid"] = {
    "column_names": columns,
    "row_names": ["01"],
    "cells": {
        "VAC,01": {"device": "r1/vac/alarm-01".upper()},
        "MAG,01": {"device": "r1/mag/alarm-01".upper()},
        "WAT,01":{"device":"r1/wat/alarm-01".upper()},

    }
}


# Beamline FE
config["grid"]["cells"]["VAC<br>FE,107"]["grid"]["cells"]["FE,S"] =\
     {"device": "B107A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,108"]["grid"]["cells"]["FE,S"] =\
     {"device": "B108A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,110"]["grid"]["cells"]["FE,S"] =\
     {"device": "B110A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,111"]["grid"]["cells"]["FE,S"] =\
     {"device": "B111A/VAC/FE-01"}
config["grid"]["cells"]["VAC<br>FE,112"]["grid"]["cells"]["FE,S"] =\
     {"device": "B112A/VAC/FE-01"}

# Beamline VGFA
config["grid"]["cells"]["VAC<br>VGFA,107"]["grid"]["cells"]["VGFA,S"] =\
     {"device": "tango://b-v-flexpes-csproxy-0:10000/b107a-fe/vac/vgfa-02".upper()}
config["grid"]["cells"]["VAC<br>VGFA,108"]["grid"]["cells"]["VGFA,S"] =\
     {"device": "tango://b-v-species-csproxy-0:10000/b108a-fe/vac/vgfa-02".upper()}
config["grid"]["cells"]["VAC<br>VGFA,110"]["grid"]["cells"]["VGFA,S"] =\
     {"device": "tango://b-v-bloch-csproxy-0:10000/b110a-fe/vac/vgfa-02".upper()}
config["grid"]["cells"]["VAC<br>VGFA,111"]["grid"]["cells"]["VGFA,S"] =\
     {"device": "tango://b-v-maxpeem-csproxy-0:10000/b111a-fe/vac/vgfa-02".upper()}
config["grid"]["cells"]["VAC<br>VGFA,112"]["grid"]["cells"]["VGFA,S"] =\
     {"device": "tango://b-v-finest-csproxy-0:10000/b112a-fe/vac/vgfa-02".upper()}

columns = ["PSS"]
config["grid"]["cells"]["PSS,Alarm"]["grid"] = {
    "column_names": columns,
    "row_names": ["01"],
    "cells": {
        "PSS,01": {"device": "R1/PSS/ALARM"},

    }
}

#Add global CTL device for feedback
columns = ["FB", "FF","SOFB", "FOFB"]
config["grid"]["cells"]["CTL,Global"]["grid"] = {
    "column_names": columns,
    "row_names": ["01", "02"],
    "cells": {
        "FB,01": {"device": "r1/ctl/fb-03".upper()},
        "FF,01": {"device": "r1/ctl/fpff-01".upper()},
        "FOFB,01": {"device": "r1/ctl/fofb-01".upper()},
        "FOFB,02": {"device": "r1/ctl/fofb-02".upper()},
        "SOFB,01": {"device": "r1/ctl/sofb-01".upper()},
        "SOFB,02": {"device": "r1/ctl/sofb-02".upper()},
    }
}

# ID
for ach in [7, 8, 10, 11, 12]:
    achromat = "1%02d" % ach
    locker_device = "R1-%sS/ID/LOCKER-01" % achromat
    config["grid"]["cells"]["ID,%s" % achromat]["grid"] = {
        "column_names": ["LOCKER"],
        "row_names": ["01"],
        "cells": {
            "LOCKER,01" : { 
                "device": locker_device
            }
        }
    }

# ID correctors
config["grid"]["cells"]["MAG,107"]["grid"]["cells"]["COID,S"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R1-D110710CAB41/MAG/PSIA-09"},
            "FF01,02": {"device": "R1-D110710CAB41/MAG/PSIA-10"},
            "FF01,03": {"device": "R1-D110710CAB41/MAG/PSIA-11"},
            "FF01,04": {"device": "R1-D110710CAB41/MAG/PSIA-12"},
        }
    }
}
config["grid"]["cells"]["MAG,108"]["grid"]["cells"]["COID,S"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R1-D110710CAB41/MAG/PSIA-01"},
            "FF01,02": {"device": "R1-D110710CAB41/MAG/PSIA-02"},
            "FF01,03": {"device": "R1-D110710CAB41/MAG/PSIA-03"},
            "FF01,04": {"device": "R1-D110710CAB41/MAG/PSIA-04"},
        }
    }
}
config["grid"]["cells"]["MAG,110"]["grid"]["cells"]["COID,S"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01", "FF03"],
        "row_names": ["01", "02", "03", "04", "05", "06", "07", "08"],
        "cells": {
            "FF01,01": {"device": "R1-D110710CAB41/MAG/PSIA-13"},
            "FF01,02": {"device": "R1-D110710CAB41/MAG/PSIA-14"},
            "FF01,03": {"device": "R1-D110710CAB41/MAG/PSIA-15"},
            "FF01,04": {"device": "R1-D110710CAB41/MAG/PSIA-16"},
            "FF03,01": {"device": "R1-D110710CAB41/MAG/PSIA-17"},
            "FF03,02": {"device": "R1-D110710CAB41/MAG/PSIA-18"},
            "FF03,03": {"device": "R1-D110710CAB41/MAG/PSIA-19"},
            "FF03,04": {"device": "R1-D110710CAB41/MAG/PSIA-20"},
            "FF03,05": {"device": "R1-D110710CAB41/MAG/PSIA-21"},
            "FF03,06": {"device": "R1-D110710CAB41/MAG/PSIA-22"},
            "FF03,07": {"device": "R1-D110710CAB41/MAG/PSIA-23"},
            "FF03,08": {"device": "R1-D110710CAB41/MAG/PSIA-24"},
        }
    }
}
config["grid"]["cells"]["MAG,111"]["grid"]["cells"]["COID,S"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01"],
        "row_names": ["01", "02", "03", "04"],
        "cells": {
            "FF01,01": {"device": "R1-D111210CAB41/MAG/PSIA-05"},
            "FF01,02": {"device": "R1-D111210CAB41/MAG/PSIA-06"},
            "FF01,03": {"device": "R1-D111210CAB41/MAG/PSIA-07"},
            "FF01,04": {"device": "R1-D111210CAB41/MAG/PSIA-08"},
        }
    }
}
config["grid"]["cells"]["MAG,112"]["grid"]["cells"]["COID,S"] = {
    "title": "ID correctors",
    "grid": {
        "column_names": ["FF01", "FF03"],
        "row_names": ["01", "02", "03", "04", "05", "06", "07", "08"],
        "cells": {
            "FF01,01": {"device": "R1-D111210CAB41/MAG/PSIA-09"},
            "FF01,02": {"device": "R1-D111210CAB41/MAG/PSIA-10"},
            "FF01,03": {"device": "R1-D111210CAB41/MAG/PSIA-11"},
            "FF01,04": {"device": "R1-D111210CAB41/MAG/PSIA-12"},
            "FF03,01": {"device": "R1-D111210CAB41/MAG/PSIA-13"},
            "FF03,02": {"device": "R1-D111210CAB41/MAG/PSIA-14"},
            "FF03,03": {"device": "R1-D111210CAB41/MAG/PSIA-15"},
            "FF03,04": {"device": "R1-D111210CAB41/MAG/PSIA-16"},
            "FF03,05": {"device": "R1-D111210CAB41/MAG/PSIA-17"},
            "FF03,06": {"device": "R1-D111210CAB41/MAG/PSIA-18"},
            "FF03,07": {"device": "R1-D111210CAB41/MAG/PSIA-19"},
            "FF03,08": {"device": "R1-D110710CAB41/MAG/PSIA-21"},
        }
    }
}




print(json.dumps(config, indent=4, sort_keys=True))
