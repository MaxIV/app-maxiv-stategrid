from tango import DeviceProxy as Device
from tango import Database
import json


class R3Config(object):
    WAT_CLASSES = ["FlowGauge",
                   "FlowSwitch",
                   'TemperatureSensor',
                   "Eurotherm"]
    VAC_CLASSES = ['HeatAbsorber',
                   'IonPump',
                   'Thermocouple',
                   'VacuumGauge',
                   'VacuumValve']
    DIA_CLASSES = ["LiberaDeviceClass"]
    MAG_CLASSES = ['Magnet']
    TOKEN = "R3"
    ALARM_TOKEN = "B3"
    ALARM_CLASSES = ['PyAlarm',
                     "AlarmPLC"]

    # Files
    VAC_FILE_NAME = "{}_vac.json".format(TOKEN.lower())
    WAT_FILE_NAME = "{}_wat.json".format(TOKEN.lower())
    DIA_FILE_NAME = "{}_bpm.json".format(TOKEN.lower())
    MAG_FILE_NAME = "{}_magnets.json".format(TOKEN.lower())
    ALARM_FILE_NAME = "{}_alarms.json".format(TOKEN.lower())

    CIRCUIT_PROPS = ["CircuitProxies"]


class R1Config(object):
    WAT_CLASSES = ["FlowGauge",
                   "TemperatureSensor",
                   "FlowSwitch",
                   "Eurotherm"]
    VAC_CLASSES = ['HeatAbsorber',
                   'IonPump',
                   'VacuumGauge',
                   'VacuumValve']
    DIA_CLASSES = ["LiberaBrilliancePlus",
                   "Thermocouple",
                   "Motor"]
    MAG_CLASSES = ['Magnet']
    ALARM_CLASSES = ['PyAlarm',
                     'AlarmPLC']
    TOKEN = "R1"
    ALARM_TOKEN = "B1"
    # Files
    VAC_FILE_NAME = "{}_vac.json".format(TOKEN.lower())
    WAT_FILE_NAME = "{}_wat.json".format(TOKEN.lower())
    DIA_FILE_NAME = "{}_dia.json".format(TOKEN.lower())
    MAG_FILE_NAME = "{}_mag.json".format(TOKEN.lower())
    ALARM_FILE_NAME = "{}_alarms.json".format(TOKEN.lower())

    CIRCUIT_PROPS = ["MainCoilProxy", "TrimCoilProxies"]


# Init tango env
db = Database()
device_db = Device(db.dev_name())


def collect_device(class_list, token):
    device_names = []
    for cls in class_list:
        device_names += [device.upper() for device in
                         db.get_device_name("*", cls) if
                         device.upper().startswith(token)]
    return device_names


def proceed_vac(conf, save_file=False):
    # Vacuum
    vacuum_devices = {device: {} for device in
                      collect_device(conf.VAC_CLASSES, conf.TOKEN)}
    if save_file:
        with open(conf.VAC_FILE_NAME, "w") as fp:
            json.dump(vacuum_devices, fp, indent=4, sort_keys=True)
    return vacuum_devices


def proceed_wat(conf, save_file=False):
    # Wat
    wat_devices = {device: {} for device in
                   collect_device(conf.WAT_CLASSES, conf.TOKEN)}
    if save_file:
        with open(conf.WAT_FILE_NAME, "w") as fp:
            json.dump(wat_devices, fp, indent=4, sort_keys=True)
    return wat_devices


def proceed_dia(conf, save_file=False):
    # Bpm
    dia_devices = {device: {} for device in
                   collect_device(conf.DIA_CLASSES, conf.TOKEN)}

    # evrx devices to be deleted as these share same class but are not wanted in SG
    keys_to_delete = []
    for key, value in dia_devices.items():
        if any(x in key for x in ["EVRX","GDX"]):
            keys_to_delete.append(key)
    for key in keys_to_delete:
        if key in dia_devices:
            del dia_devices[key]

    if save_file:
        with open(conf.DIA_FILE_NAME, "w") as fp:
            json.dump(dia_devices, fp, indent=4, sort_keys=True)
    return dia_devices


def proceed_alarm(conf, save_file=False):
    # Alarms
    alarms_devices = {device: {} for device in
                      collect_device(conf.ALARM_CLASSES, conf.TOKEN) if
                      device.lower().startswith("r3-3") or device.lower().startswith("r1-1")}
    alarms_devices.update({device: {} for device in
                      collect_device(conf.ALARM_CLASSES, conf.ALARM_TOKEN) if
                       device.lower().startswith("b3") or device.lower().startswith("b1")})
    if save_file:
        with open(conf.ALARM_FILE_NAME, "w") as fp:
            json.dump(alarms_devices, fp, indent=4, sort_keys=True)
    return alarms_devices


def proceed_mag(conf, save_file=False):
    # Mag
    mag_devices = {device: {} for device in
                   collect_device(conf.MAG_CLASSES, conf.TOKEN)}
    # Get circuits
    sql_template = "SELECT device, value from property_device WHERE "\
        "name = '{}' and device like '{}-%/MAG/%' AND "\
        "value not like '%/MAG/CRT%'"
    sql_replies = []
    for prop in conf.CIRCUIT_PROPS:
        sql_request = sql_template.format(prop, conf.TOKEN)
        sql_replies += device_db.DbMySqlSelect(sql_request)[1]
    for mag, circ in zip(sql_replies[::2], sql_replies[1::2]):
        mag_devices[mag.upper()].setdefault("circuits", []).append(circ.upper())
    # Get trim circuit
    sql_request = "SELECT device, value from property_device WHERE name = "
    sql_request += "'magnetproxies' and device like '{}%/MAG/CRT%'"
    sql_request = sql_request.format(conf.TOKEN.upper())
    sql_reply = device_db.DbMySqlSelect(sql_request)
    for trim, mag in zip(sql_reply[1][::2], sql_reply[1][1::2]):
        mag_devices[mag.upper()]["trim"] = trim.upper()
    if save_file:
        with open(conf.MAG_FILE_NAME, "w") as fp:
            json.dump(mag_devices, fp, indent=4, sort_keys=True)
    return mag_devices


def proceed_all(conf, as_file=False):
    proceed_dia(conf, save_file=as_file)
    proceed_wat(conf, save_file=as_file)
    proceed_vac(conf, save_file=as_file)
    proceed_mag(conf, save_file=as_file)
    proceed_alarm(conf, save_file=as_file)


if __name__ == '__main__':
    print("--- R1  ---")
    proceed_all(R1Config, True)
    print("R1 done !")

    print("--- R3  ---")
    proceed_all(R3Config, True)
    print("R3 done !")
