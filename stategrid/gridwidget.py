import json
import logging
import os
import re
from threading import Lock

import tango
from PyQt5.QtWebChannel import QWebChannel

try:
    from taurus.qt import QtCore, QtGui
    from taurus.qt.QtCore import QTimer, QUrl
    from taurus.qt.qtgui.panel import TaurusWidget
except ImportError:
    from taurus.external.qt import QtCore, QtGui
    from taurus.external.qt.QtCore import QTimer, QUrl
    from taurus.qt.qtgui.container import TaurusWidget

from PyQt5.QtWebEngineWidgets import QWebEnginePage, QWebEngineView


class JSInterface(QtCore.QObject):
    """
    Interface between python and a webview's javascript.

    All methods decorated with "pyqtSlot" on this class can be called
    from the JS side.
    """

    rightclicked = QtCore.pyqtSignal(str, list)
    leftclicked = QtCore.pyqtSignal(str, str)
    started = QtCore.pyqtSignal()
    load = QtCore.pyqtSignal()
    evaljs = QtCore.pyqtSignal(str)
    lock = Lock()

    def __init__(self, frame, parent=None):
        self.frame = frame

        super(JSInterface, self).__init__(parent)
        self.evaljs.connect(self.evaluate_js)  # thread safety

    def evaluate_js(self, js):
        with self.lock:
            self.frame.runJavaScript(js)

    @QtCore.pyqtSlot()
    def start(self):
        self.started.emit()

    @QtCore.pyqtSlot()
    def reload(self):
        "Load all data."
        self.load.emit()

    @QtCore.pyqtSlot(str, str)
    def left_click(self, kind, device):
        self.leftclicked.emit(str(kind), str(device))

    @QtCore.pyqtSlot(str, str)
    def right_click(self, kind, path):
        print("right click", kind, path)
        self.rightclicked.emit(kind, [str(p) for p in path.split(";")]
        if path else [])


class LoggingWebPage(QWebEnginePage):
    """
    Use a Python logger to print javascript console messages.
    Very useful for debugging javascript...
    """

    def __init__(self, logger=None, parent=None):
        super(LoggingWebPage, self).__init__(parent)
        if not logger:
            logger = logging
        self.logger = logger

    def javaScriptConsoleMessage(self, msg, lineNumber, sourceID):
        # don't use the logger for now; too verbose :)
        print("JsConsole(%s:%d):\n\t%s" % (sourceID, lineNumber, msg))


def with_state(data, state):
    return {
        "name": data["name"], "state": state,
        "last_update": data["last_update"],
        "version": data.get("version"), "errors": data.get("errors"),
        "children": dict((dev, with_state(d, state))
                         for dev, d in data["children"].items())
    }


class GridWidget(TaurusWidget):
    """A widget containing a QWebView widget showing the states of a number of devices
    in a hierarchical way. Its model should be a JSON configuration file."""

    # Note: this is not a TaurusWidget because that caused segfaults for some reason.
    # My pet theory is that Taurus internal stuff collides with using a DeviceProxy
    # for subscribing to DATA_READY_EVENT (which Taurus does not seem to support)
    # Further investigation needed but for now we don't use any taurus features here
    # anyway.

    state = QtCore.pyqtSignal(str)
    updated = QtCore.pyqtSignal(int)

    def __init__(self, parent=None, *args):
        TaurusWidget.__init__(self, parent=parent)
        self.state.connect(self.handle_data)
        self.updated.connect(self.handle_update)
        self.last_version = self.version = -1
        self.pattern = None
        self.patch_timer = None
        self.errors = 0

    def openMenu(self, position):
        print("openMenu", position)

    def setModel(self, config):
        self.config = config
        TaurusWidget.setModel(self, config["state_device"])
        self._setup_ui()

    def start_listening(self):
        try:
            self.proxy = tango.DeviceProxy(self.config["state_device"])

            # Get initial data and start the javascript side up
            data = self.proxy.read_attribute("ChildData").value
            self.js.evaljs.emit("start(%r, %r);" % (json.dumps(self.config), data))

            # subscribe to events
            self.proxy.subscribe_event("ChildData", tango.EventType.CHANGE_EVENT,
                                       self.handle_change_event)
            self.proxy.subscribe_event("ChildData", tango.EventType.DATA_READY_EVENT,
                                       self.handle_data_ready_event)

        except tango.DevFailed as e:
            print("ERROR: Could not connect to State device %s! %s" %
                  (self.getModel(), e))
            self.js.evaluate_js("start(%r, %r);" %
                                (json.dumps(self.config), "{}"))

    def handle_change_event(self, event):
        print("handle_change_event")
        if event.attr_value:
            self.state.emit(str(event.attr_value.value))
        elif event.errors:
            self.state.emit("{}")  # we have to assume the worst..?

    def handle_data_ready_event(self, event):
        # print "data ready: %d" % event.ctr
        self.updated.emit(int(event.ctr))

    def set_filter(self, pattern):
        self.pattern = re.compile(pattern, re.IGNORECASE)

    def open(self, device):
        self.js.evaluate_js("grid.open(%r)" % str(device).strip())

    def handle_update(self, version):
        "handle arrival of partial data update"
        if version == -1:
            self.errors += 1
            if self.errors >= 3:
                self.js.evaluate_js("broken();")
                return
        self.version = version
        if not self.patch_timer:
            self.patch_timer = True
            QTimer.singleShot(100, self.make_patch)  # wait for X ms before getting patch
            # because changes tend to come in bunches

    def make_patch(self):
        "Collect all updates since the current version into a patch"
        start, end, self.last_version = (
            self.last_version, self.version, self.version)
        try:
            patch = self.proxy.GetUpdates([start, end])
            print((patch[:200]) + ("..." if len(patch) > 200 else ""))
            self.js.evaluate_js("patch('%s');" % patch)
            self.errors = 0
        except tango.DevFailed as e:
            # Apparently we could not read the patches we wanted,
            # so we'll have to get the whole data instead to be sure we're
            # not missing anything
            print(e)
            self.reload_data()
        self.patch_timer = False

    def handle_data(self, data):
        "Handle arrival of complete data."
        print("handle_data", str(data)[:100])
        decoded = json.loads(str(data))  # pretty inefficient, but this should not happen often
        if decoded:
            self.last_version = decoded.get("version", 0)
            self.errors = 0
        self.js.evaluate_js("update('%s');" % str(data))

    def reload_data(self):
        print("Forcing complete data reload")
        try:
            data = self.proxy.read_attribute("ChildData").value
            self.state.emit(data)
        except tango.DevFailed as e:
            # Hm, how do we handle this case?
            # - Should notify the user if errors persist
            print("Error reloading data from device!", e)

    def on_click(self, *args):
        pass

    def on_rightclick(self, *args):
        pass

    def _setup_ui(self):
        hbox = QtGui.QHBoxLayout(self)
        hbox.setContentsMargins(0, 0, 0, 0)
        hbox.layout().setContentsMargins(0, 0, 0, 0)
        hbox.addWidget(self._create_view())

        self.setLayout(hbox)
        self.js.started.connect(self.start_listening)
        self.js.load.connect(self.reload_data)
        self.js.leftclicked.connect(self.on_click)
        self.js.rightclicked.connect(self.on_rightclick)

    def _create_view(self):
        self.view = view = QWebEngineView(self)
        view.setPage(LoggingWebPage())
        # we'll handle menus manually
        view.setContextMenuPolicy(QtCore.Qt.PreventContextMenu)

        path = os.path.dirname(os.path.realpath(__file__))
        base_url = QtCore.QUrl().fromLocalFile(path + "/static/")

        # setup the JS interface
        frame = view.page()
        self.js = JSInterface(frame)

        # mouse interaction signals
        self.clicked = self.js.leftclicked
        self.rightClicked = self.js.rightclicked

        channel = QWebChannel(self)
        channel.registerObject('Widget', self.js)
        frame.setWebChannel(channel)

        view.loadFinished.connect(self.start_listening)

        # load the page
        # TODO: make file configurable
        view.setHtml(open(os.path.join(path, "static/index.html")).read(), base_url)

        self.view = view
        return view

    def keyPressEvent(self, e):
        super().keyPressEvent(e)
        # show inspector window
        if e.key() == QtCore.Qt.Key_F12:
            DEBUG_PORT = '5588'
            DEBUG_URL = f'[http://127.0.0.1](http://127.0.0.1/):{DEBUG_PORT}'
            os.environ['QTWEBENGINE_REMOTE_DEBUGGING'] = DEBUG_PORT
            self.inspector = QWebEngineView()
            self.inspector.setWindowTitle('Web Inspector')
            self.inspector.load(QtCore.QUrl(DEBUG_URL))
            self.view.page().setDevToolsPage(self.inspector.page())
            self.inspector.show()
