import os
import sys
import json
import time
from functools import partial
from subprocess import Popen
from threading import Thread
import pkg_resources  # part of setuptools

from stategrid.gridwidget import GridWidget

import tango
from taurus import Device
from taurus.external.qt import Qt, QtGui
from taurus.qt.qtgui.application import TaurusApplication

try:
    from taurus.qt.qtgui.panel import TaurusDevicePanel, TaurusForm, TaurusWidget
except ImportError:
    from taurus.qt.qtgui.container import TaurusWidget
    from taurus.qt.qtgui.panel import TaurusDevicePanel, TaurusForm


from functools import reduce  # forward compatibility for Python 3
import operator


# Magnet panel
try:
    from magnetpanel import MagnetPanel, TrimCoilCircuitPanel
except ImportError:
    MagnetPanel = TrimCoilCircuitPanel = None

try:
    from rftransmittergui import RfTransmitterWidget
except ImportError:
    RfTransmitterWidget = TaurusForm

SCOPE_GUI = "ctscope"
SPECTRUM_ANALYZER_GUI = "ctspectrumanalyzer"

def scope_process(scope, gui_name):
    """ Start scope gui from device name """
    # Scope gui requires server name as argument
    scope_server = tango.Database().get_device_info(scope).ds_full_name
    # split ServerName/InstanceName
    scope_server = scope_server.split("/")[1]
    # Start the gui as subprocess
    Popen([gui_name, scope_server])


def camera_process(cam):
    """Start a camera gui panel in its own process"""
    # This is a (hopefully) temporary workaround to the Taurus polling problem
    # that tends to slow down the camera UI when there are many listeners.
    print("opening camera {0} in subprocess".format(cam))
    Popen(["luxviewer", cam])


def get_cell_children(cell):
    pass

# nested obect utility functions
def getpath(nested_dict, value, prepath=()):
    """Searches for a value in a nested dict and returns the path."""
    for k, v in nested_dict.items():
        path = prepath + (k,)
        if v == value: # found value
            return path
        elif isinstance(v, dict): # v is a dict
            p = getpath(v, value, path) # recursive call
            if p is not None:
                return p

def get_by_path(root, items):
    """Access a nested object in root by item sequence."""
    return reduce(operator.getitem, items, root)

def set_by_path(root, items, value):
    """Set a value in a nested object in root by item sequence."""
    get_by_path(root, items[:-1])[items[-1]] = value


class TaurusFormExpanding(TaurusForm):

    "A TaurusForm that adapts its size to its children"

    max_vertical_size = 500

    def sizeHint(self):
        frame = self.scrollArea.widget()
        framehint = frame.sizeHint()
        hint = Qt.QSize(framehint.width(), min(self.max_vertical_size,
                                               framehint.height()))
        return hint + self.buttonBox.sizeHint() + Qt.QSize(0, 20)


T_FORM_CUSTOM_WIDGET_MAP = {
    'Motor': ('sardana.taurus.qt.qtgui.extra_pool.poolmotor.PoolMotorTV', (), {}),
    'IORegister': ('sardana.taurus.qt.qtgui.extra_pool.poolioregister.PoolIORegisterButtons', (), {})
}


class SimpleDevicePanel(TaurusWidget):

    "Minimal device panel that just shows a few attributes"

    ATTRIBUTES = ["State", "Status"]

    def __init__(self, parent=None):
        TaurusWidget.__init__(self, parent)
        vbox = QtGui.QVBoxLayout(self)
        self.setLayout(vbox)
        self._form = TaurusFormExpanding(self, withButtons=False)
        vbox.addWidget(self._form)

    def setModel(self, device):
        TaurusWidget.setModel(self, device)
        self._form.setModel(["{}/{}".format(device, attribute)
                             for attribute in self.ATTRIBUTES])


class LiberaPanel(SimpleDevicePanel):
    ATTRIBUTES = ["State", "Status"]


class MyGridWidget(GridWidget):

    class_panels = {

        "Magnet": MagnetPanel,
        "MagnetCircuit": MagnetPanel,
        "TrimCircuit": TrimCoilCircuitPanel,
        "Nutaq": None,
        "NutaqDiags": None,
        "LiberaBrilliancePlus": LiberaPanel,
        "RFtransmitter": RfTransmitterWidget,
    }

    open_panels = {}
    open_grids = {}

    # === Command menus ===

    def get_cell(self, path):
        print("get_cell", path)
        cell = self.config
        for step in path:
            cell = cell["grid"]["cells"][str(step)]
        return cell

    def get_cell_devices(self, name, cell):
        """recursively go through any children of the cell and return a list
        of devices"""
        print("get_cell_devices", name)
        children = []
        if cell.get("grid"):
            for name, data in cell["grid"].get("cells", {}).items():
                children.extend(self.get_cell_devices(name, data))
        else:
            children.append(cell["device"])
        return children

    def get_column_commands(self, path):
        "Find any commands configured for the given column"
        cell = self.get_cell(path[:-1])
        col = path[-1]
        return cell.get("grid", {}).get("column_commands", {}).get(col)

    def get_column_devices(self, path):
        "Get the list of devices in a column, recursively"
        parent = self.get_cell(path[:-1])
        cells = parent["grid"]["cells"]
        col = path[-1]
        devices = []
        for cellname, celldata in cells.items():
            if celldata["col"] == col:
                devices.extend(self.get_cell_devices(cellname, celldata))
        return devices

    # def perform_action(self, path, name, command):
    #     "Run the given command on the "
    #     cell = self.get_cell(path)
    #     children = cell["grid"]["cells"].keys()
    #     print "performing", command, "on", children
    #     progress = QtGui.QProgressDialog(
    #         "Performing command '%s'..." % command,
    #         "Abort", 0, len(children), self)
    #     progress.setWindowModality(QtCore.Qt.WindowModal)
    #     for i, child in enumerate(sorted(children)):
    #         time.sleep(1)
    #         progress.setLabelText("Performing command '%s' on %s..." %
    #                               (command, child))
    #         getattr(Device(child), command)()
    #         progress.setValue(i+1)
    #         if progress.wasCanceled():
    #             break

    def perform_command(self, command, devices, wait=0.1):
        "Run a command on some devices"
        def doit():
            for dev in devices:
                getattr(Device(dev), command)()
                time.sleep(wait)

        # OK/Cancel dialog
        dialog = QtGui.QMessageBox()
        dialog.setText("Run the command '%s' on these %d devices?" %
                       (command, len(devices)))
        dialog.setInformativeText(", ".join(devices))
        dialog.setStandardButtons(QtGui.QMessageBox.Ok |
                                  QtGui.QMessageBox.Cancel)
        dialog.setDefaultButton(QtGui.QMessageBox.Ok)
        answer = dialog.exec_()

        # Idea: we could show a list of the devices and enable the
        # user to select/deselect individual devices at will.

        if answer == QtGui.QMessageBox.Ok:
            # run the commands in the background
            Thread(target=doit).start()

    def on_click(self, kind, path):
        "Open a device panel"
        if kind == "device":
            name = path
            try:
                devclass = tango.Database().get_class_for_device(str(name))
                self.show_panel(str(name), str(devclass))
            except tango.DevFailed:
                _name = str(name).lower()
                w = TaurusDevicePanel()
                w.setModel(str(_name))
                w.closeEvent = lambda _: self.cleanup_panel(w)
                w.show()
                self.open_panels[_name] = w

    def on_rightclick(self, kind, path):
        "Attempt to open a menu of commands if defined."
        print(kind, path)
        if kind == "cell":
            name = path[-1]
            menuitems = self.get_cell(path).get("commands", [])
            if menuitems:
                menu = QtGui.QMenu(name)
                # TODO: how to add a "title" to the menu?
                cell = self.get_cell(path)
                cellname = path[-1]
                devices = sorted(self.get_cell_devices(cellname, cell))
                print(devices)
                for item in menuitems:
                    action = menu.addAction(item["name"] + " (%d)" % len(devices))
                    action.triggered.connect(
                        partial(self.perform_command, item["command"], devices))

                # TODO: use popup instead or exec_ to be async
                menu.exec_(QtGui.QCursor.pos())
        elif kind == "column":
            print("menu for column {0}".format(path))
            menuitems = self.get_column_commands(path)
            if menuitems:
                menu = QtGui.QMenu(path[-1])
                devices = sorted(set(self.get_column_devices(path)))
                print(devices)
                for item in menuitems:
                    action = menu.addAction(item["name"] + " (%d)" % len(devices))
                    action.triggered.connect(
                        partial(self.perform_command, item["command"], devices))

                # TODO: use popup instead or exec_ to be async
                menu.exec_(QtGui.QCursor.pos())

    # === Panels ===

    def show_panel(self, device, cls):

        # if cls == "StateSamurai":
        #     if device in self.open_grids:
        #         w = self.open_grids[device]
        #         if not w.isVisible():
        #             w.show()
        #         w.activateWindow()
        #         w.raise_()
        #     else:
        #         grid = MyGridWidget()
        #         grid.show()
        #         grid.setModel(self.getModel())
        #         grid.open(device)
        #         self.open_grids[device] = grid
        #     return

        if cls == "Basler":
            camera_process(device)
            return

        # if cls == 'VacuumValve':
        #     print 'Valve %s clicked' % device
        #     dev = Device(device)
        #     if dev.statusopen:
        #         dev.close()
        #     else:
        #         dev.open()
        #     return

        # if cls == 'CameraScreen':
        #     print 'Screen %s clicked' % device
        #     dev = Device(device)
        #     if dev.statusin:
        #         dev.moveout()
        #     else:
        #         dev.movein()
        #     return

        if cls == "RTOScope":
            scope_process(device, SCOPE_GUI)
            return

        if cls == "SpectrumAnalyzer":
            scope_process(device, SPECTRUM_ANALYZER_GUI)
            return

        if device in self.open_panels:
            w = self.open_panels[device]
            if not w.isVisible():
                w.show()
            w.activateWindow()
            w.raise_()
        else:
            if cls in self.class_panels:
                wclass = self.class_panels[cls]
                if wclass:
                    w = wclass()
                else:
                    # This means the class is explicitly set to None
                    # so we're not going to open any panel.
                    return
            else:
                print("No panel for device {0} of class {1}".format(device, cls))
                print("Using generic TaurusDevicePanel")
                w = TaurusDevicePanel()
            w.setModel(str(device))
            w.closeEvent = lambda _: self.cleanup_panel(w)
            w.show()
            self.open_panels[device] = w

    def cleanup_panel(self, w):
        print("cleaning up panel for", w.getModel())
        self.open_panels.pop(str(w.getModel()), None)
        w.setModel(None)
        print("done!")


def start_linac():
    main(f'{os.path.dirname(sys.modules["stategrid"].__file__)}/config/linac.json')


def start_r1():
    main(f'{os.path.dirname(sys.modules["stategrid"].__file__)}/config/r1.json')


def start_r3():
    main(f'{os.path.dirname(sys.modules["stategrid"].__file__)}/config/r3.json')


def main(config=None):
    qapp = TaurusApplication([])
    sw = MyGridWidget()
    if not config:
        config = sys.argv[1]
    with open(config) as f:
        cfg = json.load(f)

    # Mask devices from the grid
        # Get list of devices from the property: 'FilterDevice'
        filtered_device = tango.Database().get_device_property(cfg['state_device'], 'FilterDevices')['FilterDevices']

        for dev in filtered_device:
            # Find path of the device
            path_list = getpath(cfg['grid']['cells'],dev)

            if path_list:
                # Clean the Device from this Cell
                set_by_path(cfg['grid']['cells'],path_list,"")
            else:
                # Maybe soon a better log handling.
                print('WARNING: Cant find {0} device in the grid!'.format(dev))

    title = cfg.get("title", config)
    sw.setWindowTitle("StateGrid %s" % (title))
    if "window_size" in cfg:
        sw.resize(cfg["window_size"]["width"], cfg["window_size"]["height"])
    else:
        sw.resize(800, 600)
    sw.show()
    sw.setModel(cfg)
    if len(sys.argv) > 2:
        sw.set_filter(sys.argv[2])
        sw.setWindowTitle("StatusGrid %s [%s]" % (sys.argv[1], sys.argv[2]))
    qapp.exec_()


if __name__ == '__main__':
    main()
