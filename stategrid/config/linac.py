from collections import OrderedDict
from itertools import product
import re
import json

import tango

db = tango.Database()

# some helpers
def get_device_section(device):
    try:
        return device.split("/")[0].split("-")[1].upper()
    except IndexError:
        return "00"


def get_device_type(device):
    if "alarm" in device.lower():
        return device.split("/")[-2]
    return device.split("/")[-1].split("-")[0].upper()


def get_device_number(device):
    try:
        n = device.rsplit("-", 1)[-1]
    except IndexError:
        return "01"
    try:
        int(n)
        return n
    except ValueError:
        if device.upper().endswith("IPCUA"):
            return "SUM"  # this is a hack to handle the VacuumSummary devices
        else:
            return "01"

def get_cols(sec,sub):
    cols = sorted(set(get_device_type(d)
                      for d in get_devices(sec, sub)))
    if sec == "SP02" and sub == "MAG":  #hack to add the IVU FF into the SP02 MAG grid
        cols = "CODX", "CODY", "COEY", "DIPBD", "QF", None, "IVUFF"
    return cols


sections = OrderedDict([
    ("GR00", ["GR00"]),
    ("GS00", ["GS00"]),
    ("00", ["00", "S00", "G00", "K00", "L00+", "RF00+"]),
    ("01", ["01", "S01A", "S01B", "K01+", "L01+", "RF01+"]),
    ("MS1", ["MS1"]),
    ("BC1", ["BC1", "KBC1", "SLBC1+"]),
    ("MS2", ["MS2"]),
    ("02", ["02", "S02A", "S02B", "K02", "L02+", "RF02+"]),
    ("03", ["03", "S03A", "S03B", "K03", "L03+", "RF03+"]),
    ("04", ["04", "S04A", "S04B", "K04", "L04+", "RF04+"]),
    ("05", ["05", "S05A", "S05B", "K05", "L05+", "RF05+", "SPL05+"]),
    ("06", ["06", "S06A", "S06B", "K06", "L06+", "RF06+"]),
    ("07", ["07", "S07A", "S07B", "K07", "L07+", "RF07+"]),
    ("08", ["08", "S08A", "S08B", "K08", "L08+", "RF08+"]),
    ("09", ["09", "S09B", "S09B", "K09", "L09+", "RF09+", "SPL09+"]),
    ("EX1", ["EX1"]),
    ("TR1", ["TR1", "KTR1"]),
    ("10", ["10", "S10A", "S10B", "K10", "L10+", "RF10+"]),
    ("11", ["11", "S11A", "S11B", "K11", "L11+", "RF11+"]),
    ("12", ["12", "S12A", "S12B", "K12", "L12+", "RF12+"]),
    ("13", ["13", "S13A", "S13B", "K13", "L13+", "RF13+"]),
    ("14", ["14", "S14A", "S14B", "K14", "L14+", "RF14+"]),
    ("15", ["15", "S15A", "S15B", "K15", "L15+", "RF15+"]),
    ("16", ["16", "S16A", "S16B", "K16", "L16+", "RF16+"]),
    ("17", ["17", "S17A", "S17B", "K17", "L17+", "RF17+"]),
    ("18", ["18", "S18A", "S18B", "K18", "L18+", "RF18+"]),
    ("19", ["19", "S19A", "S19B", "K19", "L19+", "RF19+"]),
    ("EX3", ["EX3"]),
    ("TR3", ["TR3", "KTR3"]),
    ("MS3", ["MS3"]),
    ("BC2", ["BC2", "KBC2"]),
    ("SP02", ["SP02", "C080008"])
])

# subsystems = {"MAG": [{"class": "Magnet", "pattern": "I-*"}],
#               "VAC/IP": [{"class": "GammaSPCe", "pattern": "I-*"}],
#               "VAC/VG": [{"class": "VacuumValve", "pattern": "I-*"}],
#               "DIA/SCR": [{"class": "CameraScreen", "pattern": "I-*"},
#               "DIA": [{"class": "RTOScope", "pattern": "I-*"},
#                       {"class": "RTMScope", "pattern": "I-*"}]}


# for each column, make a list of all devices that belong there
devices = {
    "MAG": [m for m in db.get_device_exported_for_class("Magnet")
            if m.upper().startswith("I-")] +  #hack in the IVU FB magnet system
            ["I-SP02/MAG/IVUFF-01", "I-SP02/MAG/IVUFF-02"],
    "VAC/IP": [m for m in db.get_device_exported_for_class("IonPump")
               if m.upper().startswith("I-")] +
               list(db.get_device_exported("I-K*/VAC/IPB-01")),
    "VAC/VG": (list(db.get_device_exported("I-*/VAC/VG*-*")) +
               list(db.get_device_exported("I-*/VAC/BD-*"))),
    "DIA/SCR": list(db.get_device_exported("I-*/DIA/SCRN*-*")),
    "DIA/CAM": list(db.get_device_exported("I-*/DIA/CAM*-*")),
    "DIA": (list(db.get_device_exported("I-*/DIA/OSC*")) +
            list(db.get_device_exported("I-*/DIA/CT-*"))),
    "DIA/BPL": list(db.get_device_exported("I-*/DIA/BP*")),
    "MPS": (list(db.get_device_exported("I-*/*/ALARM-*"))),
    "RF": (list(db.get_device_exported("I-*/RF/MOD*")) +
           list(db.get_device_exported("I-*/RF/SGEN-*"))),
    "TIM": (list(db.get_device_exported("I-*/RF/FOUT-*")) +
            list(db.get_device_exported("I-*/RF/MO-*")) +
            list(db.get_device_exported("I-*/TIM/EVG-*")) +
            list(db.get_device_exported("I-*/TIM/EVR-*")) +
            list(db.get_device_exported("I-*/TIM/TRS-*")) +
            ["G/TIM/SEL"]),
    "WAT": (list(db.get_device_exported("I-K*/WAT/SHG-01")) +
            list(db.get_device_exported("I-K00SHG02/WAT/*")) +
            list(db.get_device_exported("I-K00SHG03/WAT/*"))),
}

# Devices that don't follow the usual naming logic regarding position
device_exceptions = {
    # Ion pumps are positioned according to where in the Klystron gallery
    # the controller is, instead of where the pump is physically installed.
    "I-KBC1/VAC/IPCUA-02": "MS1",
    "I-KBC1/VAC/IPCUA-07": "MS2",
    "I-KBC1/VAC/IPCUA-08": "MS2",
    "G/TIM/SEL": "00",
    "I-K00SHG02/WAT/DPRS-01": "GR00",
    "I-K00SHG02/WAT/FGE-01": "GR00",
    "I-K00SHG02/WAT/FGE-02": "GR00",
    "I-K00SHG02/WAT/PIDM-01": "GR00",
    "I-K00SHG02/WAT/PIDM-02": "GR00",
    "I-K00SHG02/WAT/PIDM-03": "GR00",
    "I-K00SHG02/WAT/PIDS-02": "GR00",
    "I-K00SHG02/WAT/TSE-01": "GR00",
    "I-K00SHG02/WAT/TSE-02": "GR00",
    "I-K00SHG02/WAT/TSE-03": "GR00",
    "I-K00SHG02/WAT/TSE-04": "GR00",
    "I-K00SHG02/WAT/WATP-01": "GR00",
    "I-K00SHG03/WAT/DPRS-01": "GS00",
    "I-K00SHG03/WAT/FGE-01": "GS00",
    "I-K00SHG03/WAT/FGE-02": "GS00",
    "I-K00SHG03/WAT/PIDM-04": "GS00",
    "I-K00SHG03/WAT/PIDM-05": "GS00",
    "I-K00SHG03/WAT/PIDM-06": "GS00",
    "I-K00SHG03/WAT/PIDS-02": "GS00",
    "I-K00SHG03/WAT/TSE-01": "GS00",
    "I-K00SHG03/WAT/TSE-02": "GS00",
    "I-K00SHG03/WAT/TSE-03": "GS00",
    "I-K00SHG03/WAT/TSE-04": "GS00",
    "I-K00SHG03/WAT/WATP-01": "GS00"
}


# list of regexes where matching devices should be ignored
devices_to_skip = [
    "I-02/VAC/VGMB-01", "I-BC2/VAC/VGMB-01", "I-K.*/RF/MOD-.*-COND"
]


skip_regex = re.compile("(^" + "$)|(^".join(devices_to_skip) + "$)", flags=re.IGNORECASE)


# Labels for the toplevel rows
ordered_sections = [
    "GR00", "GS00", "00", None,
    "01", None,
    "MS1", "BC1", "MS2", None,
    "02", "03", "04", "05", "06", "07", "08", "09", None,
    "EX1", "TR1", None,
    "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", None,
    "EX3", "TR3", None,
    "MS3", "BC2", None,
    "SP02"
]


# This will be the labels of the toplevel columns
ordered_subsystems = ["VAC/IP", "VAC/VG", None,
                      "RF", "TIM", None,
                      "MAG", None, "MPS", None,
                      "DIA", "DIA/BPL", "DIA/CAM", "DIA/SCR", None,
                      "WAT"]

def get_devices(sec, sub):
    "Return the list of devices belonging to the given section and subsystem"
    sub_devs = devices[sub]
    secnames = sections[sec]
    devs = [d for d in sub_devs
            if sec == device_exceptions.get(d.upper()) or
            (d not in device_exceptions and
             any([re.match(s, get_device_section(d)) for s in secnames]))
            and not skip_regex.match(d)]
    return devs


def make_config():
    # Let's build the whole config in one nested dict comprehension :O
    config = {

        "title": "Linac",
        "state_device": "I/CTL/STATE",  # the toplevel UnitedStates device
        "window_size": {"width": 1000, "height": 1000},

        "grid": {
            # Top level grid, with subsystems as columns and sections as rows
            "row_names": ordered_sections,
            "column_names": ordered_subsystems,
            "cells": {
                "{},{}".format(sub, sec): {
                    "title": "I-{}/{}".format(sec, sub),  # e.g. I-TR3/VAC
                    "grid": {
                        # Sub-grid with device yypes as columns and numbers as rows
                        # Here we mash e.g. I-01, I-K01, I-S01A into simply 01
                        "row_names": sorted(set(get_device_number(d)
                                                for d in get_devices(sec, sub))),
                        "column_names":  get_cols(sec,sub),
                        "cells": {
                            "{},{}".format(
                                get_device_type(device), get_device_number(device)): {
                                    "title": device.upper(),
                                    "device": device.upper()
                                }
                            for device in get_devices(sec, sub)
                        }
                    }
                }
                for sub, sec in product(ordered_subsystems, ordered_sections)
                if sec and sub  # make sure to skip None:s
            }
        }
    }
    return config

if __name__ == '__main__':
    print(json.dumps(make_config(), indent=4))

