var Widget;

new QWebChannel(qt.webChannelTransport,
                function(channel) { Widget = channel.objects.Widget; });


var GridViewModel = (function () {

    /* 
       This is the knockout.js "viewmodel" for the State grid. It
       represents a 2D grid where each cell can either represent a
       single device or another grid of the same kind, and so on.
       
       The input is a configuration file describing the grid. It must
       point to a UnitedStates device that watches the states of all
       devices in the grid.
    */

    // default sort order, can be overridden by config
    var statePrios = ["ON", "RUNNING", "OPEN", "EXTRACT",
                      "STANDBY", "OFF", "CLOSE", "INSERT", "MOVING", "DISABLE",
                      "INIT", "NA", "ALARM", "FAULT", "UNKNOWN"];

    // tooltip defaults
    ko.bindingHandlers.tooltip.defaultTooltipOptions = {
        style: {classes: 'mytip qtip-tipsy'}, show: {solo: true},
        position: {
            my: "bottom center", at: "top center",
            viewport: true, adjust: {method: 'shift none'},
        },
        hide: {event: "click mouseleave", effect: false}
    }    

    
    function _GridViewModel(config, data) {

        this.config = config;
        var states = {};

        for (device in data) {
            states[device] = ko.observable(data[device]);
        }

        var cells = this.cells = {},
            path = this.path = ko.observable([]);  // the "path" to the current grid

        // Setting up a dependency graph where the state of each
        // cell is dependent on all its children (if any). This means
        // that if any child is updated, the cell is also updated.
        mapCellChildrenRec(config, [], function (path) {
            var cell = getConfigForPath(config, path),
                children = getCellChildren(config, path),
                name = path.join(";"),
                cellconfig = getConfigForPath(config, path);

            if (children.length === 0 && cell.device) {
                // a cell with no children is assumed to be a device
                cells[name] = states[cell.device] || ko.computed(function () {return {state: "NA"}});
            } else if (children.length === 1 &&
                       cellconfig.grid.cells[children[0]].device) {
                // a cell with only one child is replaced by the child
                var device = cellconfig.grid.cells[children[0]].device;
                cellconfig.device = device;  // override cell title
                cells[name] = states[device] || ko.computed(function () {return {state: "NA"}});
            } else if (children.length > 1) {
                // cells that have children will be computed with its children
                // as dependencies. The children in turn may have dependencies,
                // and so on.
                cells[name] = ko.computed(function () {
                    return computeCell(
                        children.map(function (d) {
                            var name = getCellName(path, d);
                            return cells[name]
                        }), cellconfig);
                }).extend({ rateLimit: 50 });  // is there a point to this?
            }
        });

        // get the rows of the current grid
        this.getGridRows = function () {
            return getGrid(config, path()).row_names;            
        };

        // get the columns in the current grid
        this.getGridColumns = function () {
            return getGridColumns(config, path());
        };

        // get a cell in the current grid
        this.getCell = function (col, row) {
            var name = getCellName(path(), makeName(col, row)),
                cell = cells[name];
            return cell? cell() : {};
        };

        // get the configuration for the current grid
        this.getConfig = function (col, row) {
            var cfg = getConfigForPath(config, path());
            return cfg.grid.cells[makeName(col, row)] || {};
        };

        // enter a cell in the current grid
        this.clickCell = function (col, row) {
            var name = makeName(col, row),
            children = getCellChildren(config, path().concat(name));
            if (children.length > 1) {
                // enter into a deeper grid
                path(path().concat(name));
            } else {
                // there are no children, let the widget decide
                var cfg = getConfigForPath(config, path().concat(name));
                //Widget && Widget.left_click("cell", path().concat(cfg.device).join(";"));
                if (cfg.device)
                    Widget && Widget.left_click("device", cfg.device);
            }
        };

        this.cellMenu = function (col, row) {
            var name = makeName(col, row);
            var cfg = getConfigForPath(config, path().concat(name));
            Widget && Widget.right_click("cell", path().concat(cfg.device || name)
                                         .join(";"));
        };
        
        // exit to a cell in the current path
        // TODO: this is a but hacky...
        this.exitCell = function (cellName) {
            path(path().slice(0, path().indexOf(cellName)+1));
        };

        // update the state data
        this.update = function (data) {
            for (device in data) {
                try {
                    states[device](data[device]);
                } catch(e) {
                    console.log("Error updating device " + device + "! Is it even in the grid?")
                }
            }
        }

        this.forget = function () {
            var now = (new Date()).getTime() / 1000;
            var data = {state: "NA", last_update: now};
            Object.keys(states).forEach(function(device) {
                states[device](data);
            });
        }
        
        // get the data for the cell at the given index in the path
        this.getCellForPathStep = function(index) {
            var cellname = getCellName(path().slice(0, index+1), []);
            return cells[cellname]();
        }

        // get the config for the given index in the path
        this.getConfigForPathStep = function(index) {
            var subpath = path().slice(0, index+1);
            return getConfigForPath(config, subpath);
        }

    }

    
    function getConfigForPath(config, cellpath) {
        var prios = config.state_priorities || statePrios;
        cellpath.forEach(function (step) {
            config = config.grid.cells[step];
            if (config.state_priorities)
                prios = config.state_priorities;
        });
        config.state_priorities = prios;
        return config;
    };

    function makeName(col, row) {
        return col + "," + row;
    }
    

    function getCellName (path, cell) {
        return path.concat(cell).join(";")
    }

    // calculate the data (State, etc) for a cell from its children
    // TODO: this is probably pretty inefficient and gets run quite a
    // lot. Optimize me!
    function computeCell(children, config) {

        var cells = children
            .filter(function (c) {return c})  // ignore empty cells
            .map(function (c) {return c()});  // get the data

        var errors = cells.reduce(function (a, b) {
            return (a.errors || 0) + (b.errors || 0)});
        
        // count of all the cells beneath this one
        var totalCells = cells.map(function (c) {
            return c.children || 1;
        }).reduce(function (a, b) {return a+b}, 0);

        // the dominant state is the "most important" state
        var prios = config.state_priorities; 
        cells.sort(function (a, b) {
            return prios.indexOf(b.state) - prios.indexOf(a.state);
        });
        var state = cells[0].state;
        
        // get the cells that share the dominant state
        var responsibleCells = cells
            .filter(function (c) {return c.state == state;});
        // count them, including children, grandchildren, etc
        var numberOfRespCells = responsibleCells
            .map(function (c) {return c.responsible? c.responsible : 1})
            .reduce(function (a, b) {return a + b}, 0)

        // take the last update time to be the oldest update of the children
        // that have the dominant state; we want to know when the current
        // state was entered.
        var last_update = responsibleCells
            .sort(function (a, b) {
                return b.last_update - a.last_update
            })[0].last_update;

        return {state: state, last_update: last_update,
                children: totalCells, errors: errors,
                responsible: numberOfRespCells};
    }

    function getGrid(config, path) {
        if (path.length == 0)
            return config.grid;
        return getGrid(config.grid.cells[path[0]], path.slice(1));
    }

    function getGridColumns(config, path) {
        return getGrid(config, path).column_names;
    }

    function mapCellChildrenRec(config, path, func) {
        // Recursively map a function over all children of a cell
        var grid = getGrid(config, path);
        children = [];
        if (!grid) {
            func(path, null);
            return [path[path.length-1]];
        }
        Object.keys(grid.cells).forEach(function (cell) {
            children = children.concat(
                mapCellChildrenRec(config, path.concat(cell), func));

        });
        func(path, children);
        return children;
    }

    function getCellChildren(config, path) {
        var grid = getGrid(config, path);
        if (!grid) {return []};
        children = Object.keys(grid.cells);
        return children;
    }

    return _GridViewModel;

})();
