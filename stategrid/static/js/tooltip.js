Tooltip.defaults.auto = 1;
Tooltip.defaults.spacing = 0;

var gTooltip = new Tooltip();
var gTooltipOwner = null;

var TooltipMixin = {

    componentDidMount: function() {
        var el = this.getDOMNode();
        el.addEventListener('mouseover', this.mouseenter);
        el.addEventListener('mouseout', this.mouseleave);
    },
    componentDidUpdate: function() {
        // We only care if the tooltip is shown and we are the owner.
        if (!gTooltip.hidden && gTooltipOwner === this && this.tooltipContent) {
            this.update(this.tooltipContent());
        }
    },
    componentWillUnmount: function() {
        var el = this.getDOMNode();
        if (gTooltipOwner === this) {
            gTooltip.detach().hide();
            gTooltipOwner = null;
        }
        el.removeEventListener('mouseover', this.mouseenter);
        el.removeEventListener('mouseout', this.mouseleave);
    },

    mouseenter: function() {
        // Assert ownership on mouseenter
        gTooltipOwner = this;
        if (this.tooltipContent) {
            this.update(this.tooltipContent());
        } else {
            console.warn("Component has TooltipMixin but does not provide tooltipContent()");
        }
    },
    mouseleave: function() {
        // Hide the tooltip only if we are still the owner.
        if (gTooltipOwner === this) {
            gTooltip.detach().hide();
            gTooltipOwner = null;
        }
    },

    update: function(content) {
        var el = this.getDOMNode();
        React.render(content, gTooltip.element, function() {
            // Need to tell the tooltip that its contents have changed so
            // it can reposition itself correctly.
            gTooltip.attach(el).show().updateSize().place('top');
        });
    }
}
